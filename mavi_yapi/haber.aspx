﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="haber.aspx.cs" Inherits="mavi_yapi.haber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="icsayfa-serit">
        <p>HABERLER</p>
    </div>
    <div class="ic-govde">
        <asp:Repeater runat="server" ID="rptHaberler">
            <ItemTemplate>
                <div class="haber-cont">
                    <div class="haber-baslik"><a href="<%#Eval("link") %>" style="color:#005580"><%#Eval("baslik")%></a> </div>
                    <div class="haber-icerik"><%#Eval("icerik")%></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
