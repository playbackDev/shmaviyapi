﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpProje_duzenle : cBase
    {
        int katID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"]!=null)
            {
                katID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var kategoribilgisi = db.maviyapi_proje.Where(o=>o.id==katID).FirstOrDefault();
                    if (kategoribilgisi!=null)
                    {
                        txtPrjBaslik.Text = kategoribilgisi.baslik;
                        txtPrjIcerik.Text = kategoribilgisi.icerik;
                        drpPrjTip.SelectedValue = kategoribilgisi.tip.ToString();
                        if (kategoribilgisi.adres != null) {
                            txtAdres.Text = kategoribilgisi.adres.ToString();
                        
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("cpProje.aspx");
            }
        }

        protected void btnKAtegoriDuzenle_Click(object sender, EventArgs e)
        {
            var kategoribilgisi = db.maviyapi_proje.Where(o => o.id == katID).FirstOrDefault();
            
            if (txtPrjBaslik.Text!= "")
                kategoribilgisi.baslik = txtPrjBaslik.Text;
            if (txtPrjIcerik.Text != "")
                kategoribilgisi.icerik = txtPrjIcerik.Text;
            kategoribilgisi.tip = Convert.ToInt16(drpPrjTip.SelectedValue);
            if (txtAdres.Text!="")
            {
                kategoribilgisi.adres = txtAdres.Text;
            }


            if (fileAnaKategori.HasFile)
            {
                try
                {
                    File.Delete(Server.MapPath("~/uploads/projeler/kapak/" + kategoribilgisi.resim));
                    fileAnaKategori.SaveAs(Server.MapPath("~/uploads/projeler/kapak/" + fileAnaKategori.FileName));
                    ResizeImage("~/uploads/projeler/kapak/" + fileAnaKategori.FileName, 200, 150);
                    kategoribilgisi.resim = fileAnaKategori.FileName;
                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("cpProje.aspx");
                    }
   
                }
                catch (Exception ex)
                {
                    ltrErrorText.Text = "Hata:" +ex.Message;
                    pnlGeneralError.Visible = true;
                }

            }
            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpProje.aspx");
            }

        }
    }
}