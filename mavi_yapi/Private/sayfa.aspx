﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="sayfa.aspx.cs" Inherits="mavi_yapi.Private.sayfa" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h5><asp:Literal ID="ltrSayfaadi" Text="" runat="server" />  </h5>
    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
    <table class="table table-condensed">
        <tr>
            <td>İçerik</td>
            <td>
                <asp:TextBox ID="txtICerik" runat="server" TextMode="MultiLine" />
            </td>
        </tr>
        <tr>
            <td>İşlem</td>
            <td>
            <asp:Button ID="btnSayfaDuzenle" runat="server" CssClass="btn btn-success" Text="Kaydet" OnClick="btnSayfaDuzenle_Click" /></td>
        </tr>
    </table>
</asp:Content>
