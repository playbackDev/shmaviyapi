﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace mavi_yapi
{
    public static class cExtensions
    {

        public static bool IsNumeric(this object Text)
        {
            try
            {
                int number = Convert.ToInt32(Text);

                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string SifirEkle(this string Text,int basamak)
        {
            string sonuc = "";

            if (Text.Length == basamak)
            {
                sonuc = Text;
            }
            else if(Text.Length<basamak)
            {
                for (int i = 0; i < ( basamak-Text.Length); i++)
                {
                    sonuc += "0";
                }
                sonuc += Text;
            }
            
            return sonuc;
        }


        public static bool IsTckimlik(this object Text)
        {
            string tcKimlik = Text.ToString();
            int uzunluk = tcKimlik.Length;
            if (uzunluk != 11)
            {
                return false;
            }
            else
            {
                int toplam = 0;
                int[] tc = new int[11];
                for (int i = 0; i <= 10; i++)
                {
                    tc[i] = Convert.ToInt16(tcKimlik[i].ToString());
                    toplam += tc[i];
                }

                int tcIlk = tc[0];
                bool numerikmi = true;

                for (int i = 0; i < 11; i++)
                {
                    if (!tcKimlik[i].ToString().IsNumeric())
                    {
                        numerikmi = false;
                    }

                }

                bool ilkSifirmi = tc[0] == 0 ? true : false;
                bool onaEsitmi = ((tc[0] + tc[2] + tc[4] + tc[6] + tc[8]) * 7 - (tc[1] + tc[3] + tc[5] + tc[7])) % 10 == tc[9] ? true : false;
                bool onbireEsitmi = (toplam - tc[10]) % 10 == tc[10] ? true : false;

                if (numerikmi && !ilkSifirmi && onaEsitmi && onbireEsitmi)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }




        public static bool IsEmail(this string Text)
        {
            try
            {
                MailAddress m = new MailAddress(Text);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string ToEN(this string Text)
        {
            Text = Regex.Replace(Text, "ş", "s");
            Text = Regex.Replace(Text, "ı", "i");
            Text = Regex.Replace(Text, "ö", "o");
            Text = Regex.Replace(Text, "ü", "u");
            Text = Regex.Replace(Text, "ç", "c");
            Text = Regex.Replace(Text, "ğ", "g");
            Text = Regex.Replace(Text, "Ş", "S");
            Text = Regex.Replace(Text, "İ", "I");
            Text = Regex.Replace(Text, "Ö", "O");
            Text = Regex.Replace(Text, "Ü", "U");
            Text = Regex.Replace(Text, "Ç", "C");
            Text = Regex.Replace(Text, "Ğ", "G");

            return Text;
        }

        public static string ToURL(this string Text)
        {
            string title = Text;
            title = title.Trim();
            title = title.Trim('-');
            title = title.ToLower();

            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&™"".".ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (title.Contains(strChar))
                    title = title.Replace(strChar, string.Empty);
            }

            title = title.Replace(" ", "-");
            title = title.Replace("--", "-");
            title = title.Replace("---", "-");
            title = title.Replace("----", "-");
            title = title.Replace("-----", "-");
            title = title.Replace("----", "-");
            title = title.Replace("---", "-");
            title = title.Replace("--", "-");
            title = title.Replace("ü", "u");
            title = title.Replace("ğ", "g");
            title = title.Replace("ş", "s");
            title = title.Replace("ö", "o");
            title = title.Replace("ç", "c");
            title = title.Replace("ı", "i");
            title = title.Trim();
            title = title.Trim('-');

            return title;
        }

        public static int ToReverse(this int CurrentValue)
        {
            if (CurrentValue == 1)
                return 0;
            else
                return 1;
        }

        public static bool ToBool(this object Value)
        {
            if (Convert.ToInt32(Value) == 1)
                return true;
            else
                return false;
        }

        public static string ToMD5(this string ClearText)
        {
            byte[] ByteData = Encoding.ASCII.GetBytes(ClearText);
            MD5 oMd5 = MD5.Create();
            byte[] HashData = oMd5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();

            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }

            return oSb.ToString();
        }

        //public static List<ListItem> CreateHistory(string date)
        //{
        //    List<ListItem> gunler = new List<ListItem>();
        //    List<ListItem> aylar = new List<ListItem>();
        //    List<ListItem> yillar = new List<ListItem>();

        //    if (date == "gun")
        //    {
        //        for (int i = 1; i < 32; i++)
        //        {
        //            gunler.Add(new ListItem(i.ToString()));


        //        }

        //        return gunler;

        //    }
        //    else if (date == "ay")
        //    {
        //        string[] ay = new string[] { Resources.setLang.txtOcak, Resources.setLang.txtSubat, Resources.setLang.txtMart, Resources.setLang.txtNisan, Resources.setLang.txtMayis, Resources.setLang.txtHaziran, Resources.setLang.txtTemmuz, Resources.setLang.txtAgustos, Resources.setLang.txtEylul, Resources.setLang.txtEkim, Resources.setLang.txtKasim, Resources.setLang.txtAralik };

              
        //        for (int i = 0; i < ay.Length; i++)
        //        {
                   
        //            aylar.Add(new ListItem(ay[i]));
        //        }

        //        return aylar;

        //    }

        //    else
        //    {
               
        //        for (int i = 1930; i < (DateTime.Now.Year-13); i++)
        //        {
                    
        //            yillar.Add(new ListItem(i.ToString()));

        //        }
        //        return yillar;

        //    }


        //}

        //public static List<ListItem> towns() {

        //    cBase cbs = new cBase();
        //    List<ListItem> kasabalar = new List<ListItem>();

        //    var sehirler = cbs.db.Towns.Where(t => t.city_ID == 1).ToList();


        //    foreach (var item in sehirler)
        //    {
        //        kasabalar.Add(new ListItem { Text=item.town, Value=item.id.ToString() });


        //    }

        //    return kasabalar;
        
        //}

        public static bool IsTextBoxEmptyOrNull(this string Text) {

            if (String.IsNullOrEmpty(Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        
        }

        public static bool AreAllTextboxIsEmptyOrNull(this Panel ID) {

            foreach (Control item in ID.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = (TextBox)item;
                    if (String.IsNullOrEmpty(txt.Text))
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }
            return false;
        }

        public static bool ZipCode(this string Text) {
            bool zipdogrumu = Regex.IsMatch(Text, "[a-zA-Z0-9]$");

            if (zipdogrumu)  //"^[0-9A-Za-z ]+$" 
            {
                if (Text.Length < 30)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public static bool OnlyLetters(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-ZöÖçÇİışŞÜü\\s]+$");
                if (uyuyomu)
                {
                    if (Text.Length <= MaxLen)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                } 
            }
            else
            {
                return false;
            }
        }

        public static bool TCKimlik(this string Text)
        {
            bool uyuyomu = Regex.IsMatch(Text, "^[1-9]{1}[0-9]{10}$");//"^[0-9A-Za-z ]+$" 

            if (uyuyomu)
            {
                if (Text.Length > 11)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsDogum(this object Text)
        {
            try
            {
                CultureInfo ci = new CultureInfo("tr-TR",true);
                DateTime dt = Convert.ToDateTime(Text,ci);

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public static bool IsPhone(this string Text) {

            bool uyuyomu = Regex.IsMatch(Text, "[+][9]{2}[0-9]{10}$");
           
            if (uyuyomu)
            {
                return true;
            }
            else
            {
                return false;
            }



        
        }

        public static bool isNumara(this string Numara, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Numara))
            {
                bool uyuyomu = Regex.IsMatch(Numara, "^[0-9]+$");

                if (uyuyomu)
                {
                    if (Numara.Length <= MaxLen)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        public static bool isNumara(this string Numara)
        {
            if (!String.IsNullOrEmpty(Numara))
            {
                bool uyuyomu = Regex.IsMatch(Numara, "^[0-9]+$");
                if (uyuyomu)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }

        }


        public static ArrayList GetSelectedItems(this Repeater rpt, string chkBoxId)
        {
            var selectedValues = new ArrayList();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var chkBox = rpt.Items[i].FindControl(chkBoxId) as HtmlInputCheckBox;

                if (chkBox != null && chkBox.Checked) {

                    //string[] degerlen = chkBox.Attributes["class"].Split(' ');
                    selectedValues.Add(chkBox.Value);
                }

            }
            return selectedValues;
        }

        public static bool veriBagla(this Repeater rpt, IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                rpt.DataSource = veri;
                rpt.DataBind();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void BindVeri(this Repeater rpt, IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                rpt.DataSource = veri;
                rpt.DataBind();
            }
        }

        public static void BindVeri(this DropDownList drp, IEnumerable veri, string Text, string Value, int zeroVal, string zeroText)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                drp.DataTextField = Text;
                drp.DataValueField = Value;
                drp.DataSource = veri;
                drp.DataBind();
                drp.Items.Insert(zeroVal, zeroText);
            }
        }

        public static bool InputTemizle(this Panel id)
        {
            try
            {
                foreach (var item in id.Controls)
                {
                    if (item is TextBox) //TextBox için TextBox At!
                    {
                        TextBox yeni = (TextBox)item;
                        yeni.Text = "";
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

       
    }
}