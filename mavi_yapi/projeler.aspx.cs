﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class projeler : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string baslik = "<a class='prjBaslik' href='/projeler.aspx'>PROJELERİMİZ</a>";
          
            if (!Request.QueryString["tip"].IsTextBoxEmptyOrNull())
            {
                int tip = Convert.ToInt16(Request.QueryString["tip"]);
                baslik += "/<a class='baslik' href='/projeler.aspx?tip="+tip +"'>"+ (tip == 1 ? "KONUT</a>" : "TAAHHÜT</a>");
                if (!Request.QueryString["durum"].IsTextBoxEmptyOrNull())
                {
                    bool durum = Convert.ToBoolean(Request.QueryString["durum"]);
                    baslik += "/<span class='baslikSpan'>" + (durum ? "TAMAMLANAN" : "DEVAM EDEN") + "</span>";
                    var projeler = db.maviyapi_proje.Where(s => s.tip == tip && s.durum==durum&& s.is_active == true).OrderBy(s => s.sira).ToList();
                    rptProjeler.DataSource = projeler;
                    rptProjeler.DataBind();


                }
                else
                {
                    var projeler = db.maviyapi_proje.Where(s => s.tip == tip && s.is_active == true).OrderBy(s => s.sira).ToList();
                    rptProjeler.DataSource = projeler;
                    rptProjeler.DataBind();
                }
                
            }
            else
            {
                var projeler = db.maviyapi_proje.Where(s =>s.is_active == true).OrderBy(s => s.sira).ToList();
                rptProjeler.DataSource = projeler;
                rptProjeler.DataBind();
            }

            ltrBaslik.Text = baslik;

        }
    }
}