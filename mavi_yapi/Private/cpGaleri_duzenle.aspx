﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpGaleri_duzenle.aspx.cs" Inherits="mavi_yapi.Private.cpGaleri_duzenle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h5>Galeri Resim Düzenle</h5>

    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
        <strong>Dikkat!</strong> Galeri resimlerinin boyutlarının çok büyük olmamasına dikkat ediniz. <br />
        Boyutları çok büyük olan resimler sitenizin geç açılmasına neden olabilir.<br />
</div>
     <table class="table table-bordered table-striped">
            <thead>
                <tr>
               <th style="text-align: left; vertical-align: middle; width: 10%;"><asp:TextBox ID="txtIcerik" runat="server" placeholder="Resim Yazısı"/> </th>
                </tr>
                <tr>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:FileUpload ID="flArsivSlide" runat="server" /> <asp:Button ID="btnAnaSlide" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnAnaSlide_Click" /> </th>
                </tr>
            </thead>
         </table>
</asp:Content>
