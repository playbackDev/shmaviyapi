﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="anasayfa_slider.aspx.cs" Inherits="mavi_yapi.Private.anasayfa_slider" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>

      .islemm {
position: absolute;
bottom: 3px;
left: 45px;
}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <h5>Banner</h5>
     <div class="span12 basin">
            <div id="s1">
                <asp:Repeater ID="rptAnaslider" runat="server" OnItemDataBound="rptAnaslider_ItemDataBound">
                    <ItemTemplate>
                        <div style="position:relative;float:left;width:447px;margin-right:5px;margin-bottom:5px;">
                        <asp:LinkButton ID="lnkSil" CssClass="silme" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style="position: absolute; text-decoration:none; bottom:7px; color: white;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton> <div class="islemm">
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                                 <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton> <a href="slider_duzenle.aspx?id=<%#Eval("id") %>" class="btn btn-success">Düzenle</a>  </div> <img class="silik" src="/uploads/banners/<%#Eval("adi") %>" />  </div>   </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
</asp:Content>
