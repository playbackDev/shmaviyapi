﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpProjeKategori.aspx.cs" Inherits="mavi_yapi.Private.cpProjeKategori" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script>
         var acik = 0;
         $(document).ready(function () {

             $(".orderClick").click(function () {
                 var csi = $(this).attr("c");
                 if (acik != csi) {
                     $(".tler").hide();
                     $(this).parent().parent().parent().next().show();
                     acik = csi;
                 }
                 else {
                     $(this).parent().parent().parent().next().hide();
                     acik = 0;
                 }
             });
         });
    </script>
    <style>
        .ortala td, tr {text-align:center;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h5> Kategoriler</h5>

       <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
  
          <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle; width: 10%;">Yeni Kategori Ekle</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:TextBox ID="txtYeni" runat="server" CssClass="" placeholder="" Text="" />


                </th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:Button ID="btnEkle" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnEkle_Click" />
                </th>
            </tr>
        </thead>
    </table>

<asp:Panel ID="pnlKategori" runat="server">
    <table class="table table-bordered ortala">
        <thead>
            <tr>
                <th style="text-align:center;width:30px">ID</th>
                <th style="text-align:center;width:250px">Kategori Adı</th>
            
                <th style="text-align:center;">Yönetim</th>
            </tr>
        </thead>
        <tbody>
             
            <asp:Repeater ID="rptKategoriler" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("id") %></td>
                        <td><%#Eval("name") %></td>
                        <td> <div class="btn-group">
                           
                            <a href="javascript:void(0)" class="orderClick btn btn-primary" c='<%#Eval("id") %>'>Düzenle</a>
                                <a href="ozelliklerr.aspx?id=<%#Eval("id") %>" class="btn">Özellikleri İncele</a> </div> </td>
                    </tr>
                    <tr style="display:none;" class="tler">
                        <td><strong><%#Eval("id") %></strong></td>
                        <td>
                            <asp:TextBox ID="txtKategori" runat="server" CssClass=""  placeholder=""  Text='<%#Eval("name") %>' />
</td>
                        <td>
                            <asp:LinkButton ID="lnkKategoriGuncelle" Text="Kaydet" runat="server" CssClass="btn btn-primary" CommandArgument='<%#Eval("id") %>' OnCommand="lnkKategoriGuncelle_Command" /></td>
                           </tr>
                </ItemTemplate>
            </asp:Repeater>
                
        </tbody>
    </table>
 </asp:Panel>


</asp:Content>
