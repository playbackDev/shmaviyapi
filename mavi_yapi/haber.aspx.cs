﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class haber :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Request.QueryString["id"].IsTextBoxEmptyOrNull())
            //{ 
            //  int id=Convert.ToInt16(Request.QueryString["id"]);
            //  var haber = db.maviyapi_duyurular.Where(s => s.id == id).FirstOrDefault();
            //  ltrHaberBaslik.Text = haber.baslik;
            //  ltrHaberIcerik.Text = haber.icerik;
            
            //}

            var haberler = db.maviyapi_duyurular.OrderBy(s=>s.sira).ToList();
            rptHaberler.DataSource = haberler;
            rptHaberler.DataBind();

        }
    }
}