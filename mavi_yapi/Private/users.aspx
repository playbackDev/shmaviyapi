﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="mavi_yapi.panel.users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlsuccess" runat="server" Visible="false" CssClass="alert alert-success">
        <asp:Literal ID="ltrsuccess" runat="server"></asp:Literal>
    </asp:Panel>
<h5>Kullanıcı Bilgileri</h5>
Bu alandan kullanıcı bilgilerinizi güncelleyebilirsiniz.<br />
<table class="table table-bordered">
    <tr>
        <td>Kullanıcı Adınız</td>
        <td><asp:TextBox ID="txtusername" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Şifreniz</td>
        <td><asp:TextBox ID="txtpass" runat="server"></asp:TextBox></td>
    </tr>
    <tfoot>
        <td><asp:Button ID="guncelle" runat="server" CssClass="btn btn-success" Text="Güncelle" onclick="guncelle_Click" /></td>
        <td></td>
    </tfoot>
</table>
</asp:Content>
