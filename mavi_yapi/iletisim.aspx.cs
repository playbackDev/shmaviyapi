﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class iletisim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            string konu = txtKonu.Text.IsTextBoxEmptyOrNull() ? "Konu Girilmedi" : txtKonu.Text;
            bool basarili = false;
            string uyariMetni = "";

            string adSoyad = txtAdSoyad.Text.IsTextBoxEmptyOrNull() ? "Ad Soyad Girilmedi" : txtAdSoyad.Text;

            string telefon = txtTel.Text.IsTextBoxEmptyOrNull() ? "Telefon girilmedi." : txtTel.Text;
            string eMail = txtEPosta.Text.IsTextBoxEmptyOrNull() ? "E-posta girilmedi." : txtEPosta.Text;
            string mesaj = txtMesaj.Text.IsTextBoxEmptyOrNull() ? "Mesaj girilmedi." : txtMesaj.Text;



            string mail_msg = string.Empty;
            mail_msg += "<table style='border:solid 1px #808080; color:#000000; background-color:#e2e2e2;'>";
            mail_msg += "<tr><td colspan='2'><h3>Mail Bilgileri</h3></td></tr>";

            mail_msg += "<tr><td>" + "Ad Soyad: " + "</td><td>" + adSoyad + "</td></tr>";
            mail_msg += "<tr><td>" + "Konu: " + "</td><td>" + konu + "</td></tr>";
            mail_msg += "<tr><td>" + "E-Mail: " + "</td><td>" + eMail + "</td></tr>";
            mail_msg += "<tr><td>" + "IP: " + "</td><td>" + Request.UserHostAddress + "</td></tr>";
            mail_msg += "<tr><td>" + "Tarih: " + "</td><td>" + DateTime.Now + "</td></tr>";

            mail_msg += "<tr><td>" + "Mesaj : " + "</td><td>" + mesaj + "</td></tr>";
            mail_msg += "</table>";


            try
            {
                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add("bilgi@shmaviyapi.com");
                eMail = eMail.IsEmail() ? eMail : "bilgi@shmaviyapi.com";
                MailAddress mailAddress = new MailAddress(txtEPosta.Text);
                mailMsg.From = mailAddress;
                mailMsg.Subject = konu;
                mailMsg.Body = mail_msg;
                mailMsg.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient("mail.shmaviyapi.com", 587);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("bilgi@shmaviyapi.com", "00545761");
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
                uyariMetni = "Mesajınız Gönderilmiştir.";
                basarili = true;
            }
            catch
            {
                uyariMetni = "Mail Gönderilirken hata oluştu.";

            }
            


            if (basarili)
                lblUyari.Text = "<div class='alert alert-success'>" + uyariMetni + "</div>";
            else
                lblUyari.Text = "<div class='alert alert-error'>" + uyariMetni + "</div>";

        }
    }
}