﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class ozellikler : cBase
    {
        int ID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"].isNumara())
            {
                ID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var ozelliklerr = db.maviyapi_kategori_ozellik.Where(i=>i.kategori_id==ID).ToList();
                    rptOzellikler.BindVeri(ozelliklerr);
                    ltrKategoriAdi.Text = ozelliklerr.FirstOrDefault().maviyapi_kategori.name;
                }
            }
            else
            {
                Response.Redirect("/", false);
            }
        }

        protected void lnkKaydet_Command(object sender, CommandEventArgs e)
        {
            int ozellikID = Convert.ToInt32(e.CommandArgument);
            var ozellik = db.maviyapi_kategori_ozellik.Where(i=>i.id==ozellikID).FirstOrDefault();
            LinkButton lnkKaydet = (LinkButton)sender;
            TextBox txtOzellik = (TextBox)lnkKaydet.Parent.FindControl("txtOzellik");
            if (txtOzellik.Text!="")
            {
                pnlGeneralError.Visible = false;
                ozellik.ozellik = txtOzellik.Text;
                if (db.SaveChanges()>0)
                {
                    RefreshPage();
                }

            }
           
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            maviyapi_kategori_ozellik yeni = new maviyapi_kategori_ozellik();
            yeni.kategori_id = ID;
            if (txtYeni.Text!="")
            {
                pnlGeneralError.Visible = false;
                yeni.ozellik = txtYeni.Text;
                db.maviyapi_kategori_ozellik.Add(yeni);
                if (db.SaveChanges()>0)
                {
                    RefreshPage();
                }

            }
            else
            {
                ltrErrorText.Text = "Lütfen özellik girin.";
                pnlGeneralError.Visible = true;
            }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int ozellikID = Convert.ToInt32(e.CommandArgument);
            var ozellik = db.maviyapi_kategori_ozellik.Where(i => i.id == ozellikID).FirstOrDefault();
            db.maviyapi_kategori_ozellik.Remove(ozellik);
            if (db.SaveChanges()>0)
            {
                RefreshPage();
            }
        }
    }
}