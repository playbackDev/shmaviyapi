﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class proje_resimleri : cBase
    {
        int ID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"].isNumara())
            {
                ID = Convert.ToInt32(Request.QueryString["id"]);
                var slider = db.maviyapi_proje_resimleri.Where(i=>i.proje_id==ID).OrderBy(p => p.sira).ToList();
                if (!Page.IsPostBack)
                {
                    if (slider != null)
                    {
                        rptAnaslider.DataSource = slider;
                        rptAnaslider.DataBind();
                    }
                }
            }
            else
            {
                Response.Redirect("default.aspx", false);
            }


            

        }

        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
            try
            {

                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.maviyapi_proje_resimleri.Where(p => p.id == id).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();

            }
            catch { }
            finally
            {

                cBase.RefreshPage();

            }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var slideResmiSil = db.maviyapi_proje_resimleri.Where(o => o.id == id).FirstOrDefault();
            db.maviyapi_proje_resimleri.Remove(slideResmiSil);
            if (db.SaveChanges() > 0)
            {
                try
                {
                    File.Delete(Server.MapPath("~/uploads/projeler/big/" + slideResmiSil.img_path));
                    File.Delete(Server.MapPath("~/uploads/projeler/small/" + slideResmiSil.img_path));
                }
                catch
                {
                }
                finally
                {
                    RefreshPage();
                }
            }
        }

        protected void rptAnaslider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "ID") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "ID") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";
        }

        protected void btnResim_Click(object sender, EventArgs e)
        {
            maviyapi_proje_resimleri yenibanner = new maviyapi_proje_resimleri();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/projeler/big/" + randomname + ".jpg"));
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/projeler/small/" + randomname + ".jpg"));
                    ResizeImage("~/uploads/projeler/small/" + randomname + ".jpg", 120, 90);


                    yenibanner.img_path = randomname + ".jpg";
                    yenibanner.sira = 1;
                    yenibanner.proje_id = ID;
                    db.maviyapi_proje_resimleri.Add(yenibanner);
                    if (db.SaveChanges() > 0)
                    {
                        RefreshPage();
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}