﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class proje_kategorileri : cBase
    {
        int ID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"].isNumara())
            {
                ID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                   rptKategoriler.BindVeri(db.maviyapi_kategori.ToList());

                   List<olmayan_alt_ozellikler> olmayanlar = new List<olmayan_alt_ozellikler>();
                   var genel_ozelliklerr = db.maviyapi_kategori_ozellik.ToList();

                   foreach (maviyapi_kategori_ozellik item in genel_ozelliklerr)
                   {
                       var ozellikControl = db.maviyapi_kategori_ozellik_proje.Where(o => o.proje_id == ID && o.ozellik_id == item.id).FirstOrDefault();
                       if (ozellikControl == null)
                       {
                           olmayanlar.Add(new olmayan_alt_ozellikler { ID = item.id, ozellik = item.ozellik });
                       }

                   }
                
                   drpAltOzellikler.BindVeri(olmayanlar, "ozellik", "id", 0, "Seçim Yapın");

                }
            }
            else
            {
                Response.Redirect("default.aspx", false);
            }

        }

        public class olmayan_alt_ozellikler
        {
            public int ID { get; set; }
            public string ozellik { get; set; }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var ozellik_proje = db.maviyapi_kategori_ozellik_proje.Where(i=>i.id==id).FirstOrDefault();
            db.maviyapi_kategori_ozellik_proje.Remove(ozellik_proje);
            if (db.SaveChanges()>0)
            {
                RefreshPage();
            }


        }

        protected void rptKategoriler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int kategoriID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "id"));
            var ozellikler = db.maviyapi_kategori_ozellik.Where(p => p.kategori_id == kategoriID && p.maviyapi_kategori_ozellik_proje.Where(o => o.proje_id == ID).Count() > 0).Select(s => new { 
                s.ozellik,
               idd= s.maviyapi_kategori_ozellik_proje.FirstOrDefault().id
            
            }).ToList();
            Repeater rptOzellikler = (Repeater)e.Item.FindControl("rptAltOZellikler");
            rptOzellikler.BindVeri(ozellikler);
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            maviyapi_kategori_ozellik_proje yeni = new maviyapi_kategori_ozellik_proje();
            yeni.proje_id = ID;
            if (drpAltOzellikler.SelectedValue.isNumara())
            {
                pnlGeneralError.Visible = false;
                yeni.ozellik_id = Convert.ToInt32(drpAltOzellikler.SelectedValue);
                db.maviyapi_kategori_ozellik_proje.Add(yeni);
                if (db.SaveChanges()>0)
                {
                    RefreshPage();
                }


            }
            else
            {
                ltrErrorText.Text = "Lütfen özellik seçin";
                pnlGeneralError.Visible = true;
            }

        }
    }
}