﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="ozelliklerr.aspx.cs" Inherits="mavi_yapi.Private.ozellikler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5> <asp:Literal ID="ltrKategoriAdi" Text="" runat="server" /> </h5>
        <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>

     <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle; width: 10%;">Yeni Özellik Ekle</th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:TextBox ID="txtYeni" runat="server" CssClass="" placeholder="" Text="" />


                </th>
                <th style="text-align: center; vertical-align: middle; width: 10%;">
                    <asp:Button ID="btnEkle" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnEkle_Click" />
                </th>
            </tr>
        </thead>
    </table>


    <asp:Panel ID="pnlOzellikler" runat="server">
        <table class="table table-bordered">
            <caption></caption>
            <thead>
                <tr>
                    <th>Özellik</th>
                    <th>Yönetim</th>
                </tr>
            </thead>
            <tbody>


                <asp:Repeater ID="rptOzellikler" runat="server">
                    <ItemTemplate>


                        <tr>
                            <td>
                                <asp:TextBox ID="txtOzellik" runat="server" CssClass="" placeholder="" Text='<%#Eval("ozellik") %>' />
                            </td>
                            <td>
                                <div class="btn-group">
                                    <asp:LinkButton ID="lnkKaydet" Text="Kaydet" runat="server" CssClass="btn btn-primary" OnCommand="lnkKaydet_Command" CommandArgument='<%#Eval("id") %>' /> <asp:LinkButton ID="lnkSil" Text="Sil" runat="server" CssClass="btn btn-danger" CommandArgument='<%#Eval("id") %>' OnCommand="lnkSil_Command" />  </div>
                            </td>
                        </tr>

                    </ItemTemplate>
                </asp:Repeater>


            </tbody>
        </table>
    </asp:Panel>

</asp:Content>
