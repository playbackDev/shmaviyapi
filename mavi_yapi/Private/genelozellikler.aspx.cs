﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class genelozellikler : cBase
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
                if (!Page.IsPostBack)
                {
                    var ozelliklerr = db.maviyapi_genel_ozellikler.ToList();
                    rptOzellikler.BindVeri(ozelliklerr);
                }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {

            maviyapi_genel_ozellikler yeni = new maviyapi_genel_ozellikler();
           
            if (txtYeni.Text != "")
            {
                pnlGeneralError.Visible = false;
                yeni.ozellik = txtYeni.Text;
                db.maviyapi_genel_ozellikler.Add(yeni);
                if (db.SaveChanges() > 0)
                {
                    RefreshPage();
                }

            }
            else
            {
                ltrErrorText.Text = "Lütfen özellik girin.";
                pnlGeneralError.Visible = true;
            }

        }

        //protected void lnkSil_Command(object sender, CommandEventArgs e)
        //{
        //    int ozellikID = Convert.ToInt32(e.CommandArgument);
        //    var ozellik = db.maviyapi_genel_ozellikler.Where(i => i.id == ozellikID).FirstOrDefault();
        //    db.maviyapi_genel_ozellikler.Remove(ozellik);
        //    if (db.SaveChanges() > 0)
        //    {
        //        RefreshPage();
        //    }
        //}

        protected void lnkKaydet_Command(object sender, CommandEventArgs e)
        {
            int ozellikID = Convert.ToInt32(e.CommandArgument);
            var ozellik = db.maviyapi_genel_ozellikler.Where(i => i.id == ozellikID).FirstOrDefault();
            LinkButton lnkKaydet = (LinkButton)sender;
            TextBox txtOzellik = (TextBox)lnkKaydet.Parent.FindControl("txtOzellik");
            if (txtOzellik.Text != "")
            {
                pnlGeneralError.Visible = false;
                ozellik.ozellik = txtOzellik.Text;
                if (db.SaveChanges() > 0)
                {
                    RefreshPage();
                }

            }
        }

        protected void lnkAktif_Command(object sender, CommandEventArgs e)
        {
            int ozellikID = Convert.ToInt32(e.CommandArgument);
            var ozellik = db.maviyapi_genel_ozellikler.Where(i => i.id == ozellikID).FirstOrDefault();
            if (Convert.ToBoolean(ozellik.is_active))
            {
                ozellik.is_active = false;
            }
            else
            {
                ozellik.is_active = true;
            }

            if (db.SaveChanges()>0)
            {
                RefreshPage();
            }
        }

        protected void rptOzellikler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton atifbutton = (LinkButton)e.Item.FindControl("lnkAktif");
            bool aktif = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem,"is_active"));
            if (aktif)
            {
                atifbutton.Text = "Aktif";
                atifbutton.CssClass = "btn btn-success";
            }
            else
            {
                atifbutton.Text = "Pasif";
                atifbutton.CssClass = "btn btn-danger";
            }

        }
    }
}