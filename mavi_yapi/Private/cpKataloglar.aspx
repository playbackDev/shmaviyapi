﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpKataloglar.aspx.cs" Inherits="mavi_yapi.Private.cpKataloglar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h5>Galeri</h5>
    <div class="span12 basin">
        <div id="s1">
            <asp:Repeater ID="rptAnaslider" runat="server" OnItemDataBound="rptAnaslider_ItemDataBound">
                <ItemTemplate>
                    <div style="border-bottom:solid #808080 1px; width:500px; padding:10px 10px 10px 10px; margin-right: 5px; margin-bottom: 5px;">
                      
                        <div class="islemm">  <asp:LinkButton ID="lnkSil" CssClass="btn btn-danger" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style=" text-decoration: none; bottom: 7px; color: black;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton>
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                            <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                            <a href="cpKatalog_duzenle.aspx?id=<%#Eval("id") %>" class="btn btn-success">Düzenle</a>
                        </div>
                        <div style="font-size:15px; color:#ff6a00; font-weight:bold; margin-top:10px;">

                             <%#Eval("baslik") %>(<%#Eval("adi") %>)
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
