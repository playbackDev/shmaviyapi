﻿jQuery.fn.extend({
    SlideFade: function () {
        var current = 0;
        var slides = $(this).children();
        var len = slides.length;
        setTimeout(setInterval(function () {
            current++;
            if (current== len)
                current = 0;
        
            $(slides).fadeOut(300);
            $(slides[current]).fadeIn(300);
        }, 2000), 2000);
    }
});

