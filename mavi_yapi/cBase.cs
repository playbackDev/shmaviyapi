﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;

namespace mavi_yapi
{
    public class cBase : System.Web.UI.Page
    {
        public maviyapiEntities db = new maviyapiEntities();
   

        /// <summary>
        /// Refresh Current Page
        /// </summary>
        public static void RefreshPage()
        {
            HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }

        private static string lang = SetLang();
        public static string Lang
        {
            get { return SetLang(); }
            set { lang = SetLang(); }

        }

        public cBase()
        {
            try
            {
                SetLang();
                HttpContext.Current.Response.Headers.Remove("Server");
            }
            catch { }
        }

        public static string SetLang()
        {
            string r = "TR";
            HttpCookie cookie = HttpContext.Current.Request.Cookies["lang"];

            if (cookie != null && cookie.Value != null)

                //if (HttpContext.Current.Request.QueryString["lang"] != null)
                // {
                switch (cookie.Value)
                {
                    case "tr-TR":
                        r = "TR";
                        break;
                    case "en-US":
                        r = "EN";
                        break;
                    
                }
            // }

            // set culture
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");

            return r;
        }

        public static void PanelMessageBox(string Message)
        {
            HttpContext.Current.Response.Write("<script>alert('" + Message + "');</script>");
        }

        public static string CreateRandomValue(int Length, bool CharactersB, bool CharactersS, bool Numbers, bool SpecialCharacters)
        {
            string characters_b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string characters_s = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "0123456789";
            string special_characters = "-_*+/";
            string allowedChars = String.Empty;

            if (CharactersB)
                allowedChars += characters_b;

            if (CharactersS)
                allowedChars += characters_s;

            if (Numbers)
                allowedChars += numbers;

            if (SpecialCharacters)
                allowedChars += special_characters;

            char[] chars = new char[Length];
            Random rd = new Random();

            for (int i = 0; i < Length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static bool ResizeImage(string imagepath, int w, int h)
        {
            try
            {
                System.Drawing.Image resmimiz = byteArrayToImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(imagepath)));
                Bitmap b = new Bitmap(w, h);
                Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(resmimiz, 0, 0, w, h);
                g.Dispose();

                b.Save(HttpContext.Current.Server.MapPath(imagepath));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static System.Drawing.Image cropImage(System.Drawing.Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);

            return (System.Drawing.Image)(bmpCrop);
        }

        private static System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);

            return GetHexaDecimal(inputbytes);
        }

        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;

            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }

            return s.ToString();
        }

        public static void RedirectAndPOST(Page page, string destinationUrl, NameValueCollection data)
        {
            string strForm = PreparePOSTForm(destinationUrl, data);
            page.Controls.Add(new LiteralControl(strForm));
        }

        public static String PreparePOSTForm(string url, NameValueCollection data)
        {
            string formID = "PostForm";
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");

            foreach (string key in data)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + data[key] + "\">");
            }

            strForm.Append("</form>");
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language=\"javascript\">");
            strScript.Append("var v" + formID + " = document." + formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");

            return strForm.ToString() + strScript.ToString();
        }

        public static bool SendEmail(string from, string Subject, string Message)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add("mail@subilgi.biz");

                MailAddress mailAddress = new MailAddress(from);
                mailMsg.From = mailAddress;
                mailMsg.Subject = Subject;
                mailMsg.Body = Message;
                mailMsg.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient("webmail.subilgi.biz",587);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("mail@subilgi.biz", "43613938");
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);

                return true;
            }
            catch
            {
                return false;
            }
        }



    }
}