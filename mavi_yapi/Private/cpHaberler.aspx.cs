﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpHaberler : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var duyurular = db.maviyapi_duyurular.ToList();
            rptprj.DataSource = duyurular;
            rptprj.DataBind();
        }

        protected void btnsil_Command(object sender, CommandEventArgs e)
        {
            int duyuruID = Convert.ToInt16(e.CommandArgument.ToString());
            var duyuru = db.maviyapi_duyurular.Where(d => d.id == duyuruID).FirstOrDefault();
            db.maviyapi_duyurular.Remove(duyuru);

            if (db.SaveChanges() > 0)
                RefreshPage();
        }
    }
}