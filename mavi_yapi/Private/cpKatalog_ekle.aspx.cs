﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpKatalog_ekle :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            maviyapi_katalog yenikatalog = new maviyapi_katalog();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/katalog/" + randomname + ".pdf"));
                    
                    if (txtBaslik.Text != "")
                        yenikatalog.baslik = txtBaslik.Text;

                    yenikatalog.adi = randomname + ".pdf";
                    yenikatalog.sira = 1;
                    yenikatalog.is_active = true;
                    db.maviyapi_katalog.Add(yenikatalog);
                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("cpKataloglar.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}