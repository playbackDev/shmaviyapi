﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="mavi_yapi._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--Kayan haber--%>
    <link href="/assets/jquery_news_ticker/styles/ticker-style.css" rel="stylesheet" />
    <script src="/assets/jquery_news_ticker/includes/jquery.ticker.js"></script>
    <script src="/assets/jquery_news_ticker/includes/site.js"></script>
    <%--Banner--%> 
   
    <link href="/assets/plugins/jquery_basic_slider/bjqs.css" rel="stylesheet" />
    <link href="/assets/plugins/jquery_basic_slider/demo.css" rel="stylesheet" />
    <script src="/assets/plugins/jquery_basic_slider/js/bjqs-1.3.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('#banner-fade').bjqs({
                height: 480,
                width: 897,
                responsive: true
            });

            $('#banner-slide').bjqs({
                animtype: 'slide',
                height: 480,
                width: 897,
                responsive: true,
                randomstart: true
            });
        });
                    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

                    <div id="banner-fade">

                        <!-- start Basic Jquery Slider -->
                        <ul class="bjqs">
                            <asp:Repeater runat="server" ID="rptBanner">
                                <ItemTemplate>
                                    <li><a href="<%#Eval("link")%>">
                                        <img src="/uploads/banners/<%#Eval("adi")%>" title="<%#Eval("baslik")%>"></a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                        <!-- end Basic jQuery Slider -->
                    </div>
                    <!-- End outer wrapper -->
                    <div class="galeri-content">
                       
                        <ul class="galeri">
                            <asp:Repeater runat="server" ID="rptGaleri">
                                <ItemTemplate>
                                    <li><a href="/uploads/galeri/<%#Eval("adi")%>" rel="prettyPhoto[maviyapi]">
                                        <img class="galeri-img" src="/uploads/galeri/<%#Eval("adi")%>" height="90" width="120" /></a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                       
                    </div>
                    <ul id="js-news" class="js-hidden">
                        <asp:Repeater runat="server" ID="rptHaberScript">
                            <ItemTemplate>
                                <li class="news-item">
                                    <a href="/Haberler"><%#Eval("baslik")%></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <noscript>
                        <ul id="no-js-news">
                            <asp:Repeater runat="server" ID="rptHaberNoScript">
                                <ItemTemplate>
                                    <li class="news-item">
                                        <a href="/Haberler"><%#Eval("baslik")%></a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </noscript>
</asp:Content>
