﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class hakkimizda : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var hakkimizda = db.maviyapi_sayfalar.Where(s => s.id == 1).FirstOrDefault();
            ltrHakkimizda.Text = hakkimizda.icerik;
        }
    }
}