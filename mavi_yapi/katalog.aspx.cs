﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class katalog : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.QueryString["id"].IsTextBoxEmptyOrNull())
            {
                int id = Convert.ToInt16(Request.QueryString["id"]);
                var katalog = db.maviyapi_katalog.Where(s=>s.id==id).FirstOrDefault().adi;
               
                string obj= "<object data='/uploads/katalog/"+katalog.ToString()+"' type='application/pdf' style='height:700px; width:100%;'>"
                    + "<p>" 
                    + "pdf viever desteklenmiyor. dosyayı indirmek için"
                    + "<a href='/uploads/katalog/" + katalog.ToString() + "'>tıklayın</a>"
                    + "</p>"
                    + "</object>";
                    ltrPdfViewer.Text=obj;
            
            
            }

        }
    }
}