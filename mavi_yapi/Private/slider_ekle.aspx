﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="slider_ekle.aspx.cs" Inherits="mavi_yapi.Private.slider_ekle" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5>Slide Ekle</h5>

    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
       <strong>Dikkat!</strong> Slider resimlerini 880x340 px veya aynı orantıda yükleyin. <br />
       
</div>

       <table class="table table-bordered table-striped">
            <thead>                
                <tr>
                    <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:TextBox ID="txtLink" runat="server" placeholder="Slide linki" /> </th>
                </tr>
                <tr> <th>                
                    <asp:TextBox ID="txtIcerik" runat="server" placeholder="Slider Yazısı"  />
                     <span style="color:red;">Maximum 50 Karakter girin!</span>   </th>
                </tr>
                <tr>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:FileUpload ID="flArsivSlide" runat="server" /> <asp:Button ID="btnAnaSlide" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnAnaSlide_Click" /> </th>
                </tr>
            </thead>
         </table>
</asp:Content>
