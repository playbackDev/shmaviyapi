﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mavi_yapi.panel
{
    public partial class Login : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null)
                    {
                        Response.Cookies["urlreferrer"].Value = Request.UrlReferrer.ToString();
                    }
                    else
                    {
                        Response.Cookies["urlreferrer"].Value = "Default.aspx";
                    }
                }
            }
            catch { }

            if (Request.Cookies["panellogin"] != null) Response.Redirect("Default.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            

                var user = db.maviyapi_admin.Where(d => d.kullaniciAdi == txtUser.Text && d.sifre == txtPass.Text).FirstOrDefault();
                
                if (user != null)
                {
                    Response.Cookies["panellogin"].Value = "ok";
                    Response.Cookies["user"].Value = "admin";

                    Session["userID"] = user.ID.ToString();
                    Response.Redirect("Default.aspx");

                    
                }

                else
                {
                    ltrError.Text = "Hata Oluştu";
                }
            
         
        }
    }
}