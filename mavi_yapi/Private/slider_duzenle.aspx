﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="slider_duzenle.aspx.cs" Inherits="mavi_yapi.Private.slider_duzenle" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5>Slide Düzenle</h5>

    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
        <strong>Dikkat!</strong> Slider resimlerini 880x340 px veya aynı orantıda yükleyin. <br />
</div>
     <table class="table table-bordered table-striped">
            <thead>
                
                <tr>
                <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:TextBox ID="txtLink" runat="server" placeholder="Slider linki" /> </th>
                </tr>
                <tr>
               <th style="text-align: left; vertical-align: middle; width: 10%;"><asp:TextBox ID="txtIcerik" runat="server" placeholder="Slider Yazısı"/> </th>
                </tr>
                <tr>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:FileUpload ID="flArsivSlide" runat="server" /> <asp:Button ID="btnAnaSlide" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnAnaSlide_Click" /> </th>

                </tr>
            </thead>
         </table>
</asp:Content>
