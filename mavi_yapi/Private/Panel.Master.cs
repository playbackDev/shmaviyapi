﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class Panel : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["panellogin"] == null) Response.Redirect("Login.aspx");
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            Response.Cookies["panellogin"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["user"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("Login.aspx");
        }
    }
}