﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpHaberler.aspx.cs" Inherits="mavi_yapi.Private.cpHaberler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h5>Haberler</h5>
    <table class="table table-hover">
        <asp:Repeater ID="rptprj" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%#Eval("baslik") %></td>
                    <td><asp:LinkButton ID="btnsil" runat="server" Text="Sil" CssClass="btn btn-danger" OnCommand="btnsil_Command" CommandArgument='<%# Eval("id") %>'></asp:LinkButton> <a class="btn btn-success" href="cpHaber_duzenle.aspx?id=<%# Eval("id") %>">Düzenle</a></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td><a href="cpHaber_ekle.aspx" class="btn btn-success"><i class="icon-tags"></i>Haber Ekle</a></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
