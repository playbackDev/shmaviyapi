﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class anasayfa_slider : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var slider = db.maviyapi_banner.OrderBy(p=>p.sira).ToList();
            if (!Page.IsPostBack)
            {
                if (slider != null)
                {
                    rptAnaslider.DataSource = slider;
                    rptAnaslider.DataBind();
                }
            }
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var slideResmiSil = db.maviyapi_banner.Where(o => o.id == id).FirstOrDefault();
            db.maviyapi_banner.Remove(slideResmiSil);
            if (db.SaveChanges() > 0)
            {
                try
                {
                    File.Delete(Server.MapPath("~/uploads/banners/" + slideResmiSil.adi));
                }
                catch
                {
                }
                finally
                {
                    RefreshPage();
                }
            }
        }

       

        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
           
            try
            {

                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.maviyapi_banner.Where(p => p.id == id).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();

            }
            catch { }
            finally
            {

                cBase.RefreshPage();

            }
        }

        protected void rptAnaslider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "ID") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "ID") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";
        }
    }
}