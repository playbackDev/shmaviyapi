﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class slider_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            maviyapi_banner yenibanner = new maviyapi_banner();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/banners/" + randomname + ".jpg"));
                    ResizeImage("~/uploads/banners/" + randomname + ".jpg", 896, 480);

                    if (txtIcerik.Text != "")
                        yenibanner.baslik = txtIcerik.Text;
                    if (txtLink.Text != "")
                        yenibanner.link = txtLink.Text;

                    yenibanner.adi = randomname + ".jpg";
                    yenibanner.sira = 1;
                    db.maviyapi_banner.Add(yenibanner);
                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("anasayfa_slider.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}