﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpGaleri_ekle :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            maviyapi_galeri yenibanner = new maviyapi_galeri();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/galeri/" + randomname + ".jpg"));
                    if (txtIcerik.Text != "")
                        yenibanner.baslik = txtIcerik.Text;
                    
                    yenibanner.adi = randomname + ".jpg";
                    yenibanner.sira = 1;
                    db.maviyapi_galeri.Add(yenibanner);
                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("cpGaleri.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}