﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpHaber_duzenle.aspx.cs" ValidateRequest="false" Inherits="mavi_yapi.Private.cpHaber_duzenle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Literal Text="" runat="server" ID="ltrUyari" />
     <h5>Haber Düzenle</h5>
     <table class="table">
        <tr>
            <td style="width:250px;">Haber Başlık</td>
            <td><asp:TextBox ID="txtbaslik" runat="server"></asp:TextBox></td>
        </tr>
           <tr>
            <td style="width:250px;">Haber Link</td>
            <td><asp:TextBox ID="txtLink" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Haber İçerik</td>
            <td><asp:TextBox ID="txtIcerik" runat="server" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
         <tr>
            <td>Haber sıra</td>
            <td><asp:TextBox ID="txtSira" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnekle" runat="server" Text="Düzenle" CssClass="btn" OnClick="btnekle_Click" /></td>
        </tr>
    </table>
</asp:Content>
