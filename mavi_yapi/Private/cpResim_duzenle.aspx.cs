﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpResim_duzenle :cBase
    {
        int slideID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                slideID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var slide = db.maviyapi_resim.Where(i => i.id == slideID).FirstOrDefault();
                    if (slide != null)
                    {
                        txtBaslik.Text= slide.baslik;

                    }

                }
            }

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            var slide = db.maviyapi_resim.Where(i => i.id == slideID).FirstOrDefault();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (txtBaslik.Text != "")
            {
                slide.baslik = txtBaslik.Text;
            }

            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/projeler/icerik/" + randomname + ".jpg"));
                    slide.adi = randomname + ".jpg";

                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }


            }

            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpResimler.aspx?galeriID="+slide.galeri_id);
            }

        }
    }
}