﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpProjeKategori : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rptKategoriler.BindVeri(db.maviyapi_kategori.ToList());
            }

        }

        protected void lnkKategoriGuncelle_Command(object sender, CommandEventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            TextBox txtKategori = (TextBox)lnk.Parent.FindControl("txtKategori");

            int kategoriID = Convert.ToInt32(e.CommandArgument);
            var kategori = db.maviyapi_kategori.Where(i=>i.id==kategoriID).FirstOrDefault();

            if (txtKategori.Text!="")
            {
                kategori.name = txtKategori.Text;
            }

            if (db.SaveChanges()>0)
            {
                RefreshPage();
            }

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            maviyapi_kategori yeni = new maviyapi_kategori();
            if (txtYeni.Text!="")
            {
                pnlGeneralError.Visible = false;
                yeni.name = txtYeni.Text;
                db.maviyapi_kategori.Add(yeni);
                if (db.SaveChanges()>0)
                {
                    RefreshPage();
                }

            }
            else
            {
                ltrErrorText.Text = "Lütfen kategori adı girin.";
                pnlGeneralError.Visible = true;
            }
        }

      
    }
}