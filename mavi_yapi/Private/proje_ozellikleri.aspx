﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="proje_ozellikleri.aspx.cs" Inherits="mavi_yapi.Private.proje_kategorileri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h5>Kategoriler</h5>

    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>


    <asp:Panel ID="pnlKategori" runat="server">
        <table class="table table-bordered ortala">
            <tbody> 
                <tr><td> <asp:DropDownList ID="drpAltOzellikler" runat="server">
                       
                    </asp:DropDownList></td> <td> <asp:LinkButton ID="btnEkle" Text="Ekle" runat="server" OnClick="btnEkle_Click" CssClass="btn btn-primary" />  </td>  </tr>

            <asp:Repeater ID="rptKategoriler" runat="server" OnItemDataBound="rptKategoriler_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td colspan="3">

                            <strong><%#Eval("name") %></strong>
                            <br />
                            <ul>
                                <asp:Repeater ID="rptAltOZellikler" runat="server">
                                    <ItemTemplate>
                                        <li class="lifloat">
                                            <asp:LinkButton ID="lnkSil" Text="Sil" runat="server" CommandArgument='<%#Eval("idd") %>' OnCommand="lnkSil_Command" /> <%#Eval("ozellik") %>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </ul>

                        </td>

                    </tr>
                </ItemTemplate>
            </asp:Repeater>

            </tbody>
        </table>
    </asp:Panel>


</asp:Content>
