﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpHaber_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnekle_Click(object sender, EventArgs e)
        {
            try
            {
                maviyapi_duyurular haber = new maviyapi_duyurular();
                haber.baslik = txtbaslik.Text;
                haber.icerik = txtIcerik.Text;
                haber.sira = 1;

                if (txtLink.Text!="")
                {
                    haber.link = txtLink.Text;
                }


                db.maviyapi_duyurular.Add(haber);
                if (db.SaveChanges() > 0)
                    ltrUyari.Text = "<div class='alert alert-success'>" + "İşlem başarıyla gerçekleşti." + "</div>";
            }
            catch
            {
                ltrUyari.Text = "<div class='alert alert-error'>" + "Veri Kaydederken hata oluştu." + "</div>";
            }

        }
    }
}