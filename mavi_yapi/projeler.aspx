﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="projeler.aspx.cs" Inherits="mavi_yapi.projeler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="icsayfa-serit">
        <p><asp:Literal runat="server" ID="ltrBaslik" /></p>
    </div>
    <div class="ic-govde">
        <asp:Repeater runat="server" ID="rptProjeler">
            <ItemTemplate>
                <a href="/Proje/<%#Eval("id")%>">
                    <div class="projeler-cont">
                        <img class="prj-kapak-image" src="/uploads/projeler/kapak/<%#Eval("resim")%>" />
                        <img class="prj-kapak-image-bottom" src="/assets/img/projeler-img-bottom.png" />
                        <p class="prj-baslik"><%#Eval("baslik")%></p>
                    </div>
                </a>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
