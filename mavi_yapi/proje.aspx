﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="proje.aspx.cs" Inherits="mavi_yapi.proje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/plugins/jquery_basic_slider/bjqs.css" rel="stylesheet" />
    <link href="/assets/plugins/jquery_basic_slider/demo.css" rel="stylesheet" />
    <script src="/assets/plugins/jquery_basic_slider/js/bjqs-1.3.js"></script>
    <script src="/assets/js/emre.js"></script>
    <style>
        .bjqs {
            height: 405px !important;
        }

            .bjqs li {
                height: 405px !important;
                position: absolute;
                display: none;
            }

                .bjqs li img {
                    height: 405px !important;
                }

        #banner-fade {
            height: 405px !important;
        }

        .galeri li {
            margin-top: 0px !important;
            cursor: pointer;
        }

        .galeri-content {
            position: relative;
            overflow: hidden;
            width: 541px !important;
            height: 90px !important;
        }

        .galeri {
            position: absolute;
            left: 12px;
            width: 2000px !important;
        }

        .bjqs {
            display: block !important;
        }

        .iletisimm {
            background-color: #E5F7FD;
        }

            .iletisimm tr:nth-child(odd) {
                background-color: #B2D1E1;
            }

            .iletisimm td {
                padding: 10px;
                vertical-align: middle !important;
            }
    </style>
    <script>
        //ALINACAK BAŞLA
        var resimSayisi;
        var current = 0;
        var timer;
        function timm() {
            timer = setInterval(dondur, 3000);

        }

        function dondur() {

            if (current == (resimSayisi - 1)) {
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(0).fadeIn();
                current = 0;
            }
            else {
                current++;
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(current).fadeIn();
            }
        }


        jQuery(document).ready(function ($) {

            resimSayisi = $(".bjqs li").length;
            $(".bjqs li").eq(0).fadeIn(100);

            timer = setInterval(dondur, 3000);


            $(".galeri li").click(function () {
                clearInterval(timer);
                var index = $(this).index();
                current = index;
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(index).fadeIn(function () {
                    // timer = setInterval(dondur, 3000);

                    setTimeout(timm, 500);

                });
            });
        });

        function prvImg() {
            clearInterval(timer);
            if (current == 0) {
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(resimSayisi - 1).fadeIn(function () {

                    //timer = setInterval(dondur, 3000);
                    setTimeout(timm, 500);

                });
                current = resimSayisi - 1;
            }
            else {
                current--;
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(current).fadeIn(function () {

                    //  timer = setInterval(dondur, 2000);
                    setTimeout(timm, 500);
                });
            }
        }
        function nxtImg() {
            clearInterval(timer);
            if (current == (resimSayisi - 1)) {
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(0).fadeIn(function () {

                    // timer = setInterval(dondur, 2000);
                    setTimeout(timm, 500);
                });
                current = 0;
            }
            else {
                current++;
                $(".bjqs li").fadeOut();
                $(".bjqs li").eq(current).fadeIn(function () {

                    // timer = setInterval(dondur, 2000);
                    setTimeout(timm, 500);
                });

            }
        }

        //ALINACAK BiTİŞ


        var disResim = 0;
        function nextImage() {
            resimSayisi = $(".galeri li").length;
            if (resimSayisi - 4 != disResim) { $(".galeri").animate({ left: "-=120px" }, function () { disResim++; }); }
        }
        function prevImage() {
            if (disResim > 0) { $(".galeri").animate({ left: "+=120px" }, function () { disResim--; }); }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="icsayfa-serit">
        <p>PROJELERİMİZ</p>
    </div>
    <div class="ic-govde">
        <h3>
            <asp:Literal Text="" ID="ltrBaslik" runat="server" /></h3>
        <asp:Literal Text="" ID="ltrIcerik" runat="server" />

        <div class="container">
            <div class="row">
                <div class="span7">
                    <div id="projeSlider">
                        <div id="banner-fade" class="mb20" style="position: relative;">
                            <img src="/assets/plugins/jquery_basic_slider/img/prew.png" style="position: absolute; left: -11px; z-index: 5; top: 180px; cursor: pointer;" onclick="prvImg()" />

                            <ul class="bjqs">
                                <%--<asp:Repeater runat="server" ID="rptBanner">
                                    <ItemTemplate>--%>
                                <li>
                                    <img src="http://shmaviyapi.com/uploads/projeler/big/qWD6a3tLjZ.jpg">
                                </li>
                                <li>
                                    <img src="http://shmaviyapi.com/uploads/projeler/big/1jFtT2kgKX.jpg">
                                </li>
                                <li>
                                    <img src="http://shmaviyapi.com/uploads/projeler/big/j1Jxiq1x8z.jpg">
                                </li>
                                <li>
                                    <img src="http://shmaviyapi.com/uploads/projeler/big/5g2u4uyCi3.jpg">
                                </li>
                                <%--     </ItemTemplate>
                                </asp:Repeater>--%>
                            </ul>



                            <img src="/assets/plugins/jquery_basic_slider/img/next.png" style="position: absolute; left: 510px; z-index: 5; top: 180px; cursor: pointer;" onclick="nxtImg()" />
                            <!-- end Basic jQuery Slider -->
                        </div>
                        <div class="galeri-content">
                            <img src="/assets/img/galeri-left-arrow.png" style="z-index: 10; position: absolute; left: 0px; top: 0px; cursor: pointer;" onclick="prevImage()" />
                            <ul class="galeri">
                                <%--  <asp:Repeater runat="server" ID="rptGaleri">
                                    <ItemTemplate>--%>
                                <%--        <li><img class="galeri-img" src="/uploads/projeler/small/<%#Eval("img_path")%>" height="90" width="120" /></li>--%>
                                <li>
                                    <img class="galeri-img" height="90" width="120" src="http://shmaviyapi.com/uploads/projeler/big/qWD6a3tLjZ.jpg">
                                </li>
                                <li>
                                    <img class="galeri-img" height="90" width="120" src="http://shmaviyapi.com/uploads/projeler/big/1jFtT2kgKX.jpg">
                                </li>
                                <li>
                                    <img class="galeri-img" height="90" width="120" src="http://shmaviyapi.com/uploads/projeler/big/j1Jxiq1x8z.jpg">
                                </li>
                                <li>
                                    <img class="galeri-img" height="90" width="120" src="http://shmaviyapi.com/uploads/projeler/big/5g2u4uyCi3.jpg">
                                </li>

                                <%-- </ItemTemplate>
                                </asp:Repeater>--%>
                            </ul>

                            <img src="/assets/img/galeri-right-arrow.png" style="z-index: 10; position: absolute; left: 523px; top: 0px; cursor: pointer;" onclick="nextImage()" />

                        </div>

                        <div class="mt10">

                            <h3 style="display: block; background-color: #B2D1E1; height: 32px; width: 521px; padding: 10px; line-height: 35px;">Açıklama
                            </h3>
                            <asp:Literal ID="ltrAciklama" Text="" runat="server" />

                        </div>


                    </div>
                </div>
                <div class="span4">

                    <asp:Literal ID="ltrAdres" Text="" runat="server" />
                    <br />
                    <br />

                    <table class="table-bordered maviclass">
                        <asp:Repeater ID="rptGenelOzellikler" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Eval("ozellik") %></td>
                                    <td><%#Eval("degeri") %></td>
                                </tr>

                            </ItemTemplate>
                        </asp:Repeater>
                    </table>

                </div>

            </div>

            <asp:Repeater ID="rptKategoriler" runat="server" OnItemDataBound="rptKategoriler_ItemDataBound">
                <ItemTemplate>
                    <div class="row mt20">
                        <div class="span10" style="width: 830px;">

                            <h3 style="display: block; background-color: #B2D1E1; height: 32px; width: 100%; padding: 10px; line-height: 35px;"><%#Eval("name") %></h3>
                            <div class="mt20">
                                <table class="ozelliklerr">
                                    <tr>
                                        <asp:Repeater ID="rptAltOzellikler" runat="server" OnItemDataBound="rptAltOzellikler_ItemDataBound">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Literal ID="ltrValue" Text="" runat="server" /></td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <%--    <div class="row">
                <div class="span10">
                      <h3 style="display: block; background-color: #B2D1E1; height: 32px; width:830px; padding: 10px; line-height: 35px;">Kredi Hesaplama</h3>

                    <div id="isBankFlashContainer"><object type="application/x-shockwave-flash" id="banners" name="banners" data="/uploads/isbank.swf" width="865" height="472"><param name="bgcolor" value="#ffffff"><param name="wmode" value="transparent"><param name="flashvars" value="xmlFilePath=/uploads/isbank_chart_settings.xml?v=6&amp;set_anapara=119625&amp;set_vade=120"></object></div>

                </div>

            </div>--%>

            <div class="row">
                <div class="span10">
                    <h3 style="display: block; background-color: #B2D1E1; height: 32px; width: 830px; padding: 10px; line-height: 35px;">İlan Sahibine Mesaj Gönder</h3>

                    <asp:Panel ID="pnlIlet" runat="server">
                        <table class="table table-bordered iletisimm">
                            <tbody>
                                <tr>
                                    <td>Ad Soyad</td>
                                    <td>
                                        <asp:TextBox ID="txtAd" runat="server" CssClass="" placeholder="" Text="" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>E-Posta</td>
                                    <td>
                                        <asp:TextBox ID="txtMail" runat="server" CssClass="" placeholder="" Text="" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>Telefon</td>
                                    <td>
                                        <asp:TextBox ID="txtTel" runat="server" CssClass="" placeholder="" Text="" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>Konu</td>
                                    <td>
                                        <asp:TextBox ID="txtKonu" runat="server" CssClass="" placeholder="" Text="" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>Mesaj</td>
                                    <td>
                                        <asp:TextBox ID="txtMesaj" runat="server" CssClass="" placeholder="" Text="" TextMode="MultiLine" Width="90%" Height="150" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltruyarii" Text="" runat="server" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID='lnkGonder' runat='server' CssClass="btn btn-primary" Text="Gönder" OnClick="lnkGonder_Click" />

                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </asp:Panel>




                </div>

            </div>


        </div>


    </div>
</asp:Content>
