﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="proje_resimleri.aspx.cs" Inherits="mavi_yapi.Private.proje_resimleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
    .islemm {
position: absolute;
bottom: 3px;
left: 45px;
}</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5>Proje Resimleri</h5>
    <div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
       <strong>Dikkat!</strong> Slider resimlerini 880x340 px veya aynı orantıda yükleyin. <br />
       
</div>

       <table class="table table-bordered table-striped">
            <thead>  

                <tr>
                    <th>Yeni Resim</th> <th><asp:FileUpload ID="flArsivSlide" runat="server" /> </th>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:Button ID="btnResim" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnResim_Click" /> </th>
                </tr>
            </thead>
         </table>

    <div class="span12 basin">
        <div id="s1">
            <asp:Repeater ID="rptAnaslider" runat="server" OnItemDataBound="rptAnaslider_ItemDataBound">
                <ItemTemplate>
                    <div style="position: relative; float: left; width: 447px; margin-right: 5px; margin-bottom: 5px;">
                        <asp:LinkButton ID="lnkSil" CssClass="silme" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style="position: absolute; text-decoration: none; bottom: 7px; color: white;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton>
                        <div class="islemm">
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                            <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                            <a href="cpGaleri_duzenle.aspx?id=<%#Eval("id") %>" class="btn btn-success">Düzenle</a>
                        </div>
                        <img class="silik" src="/uploads/projeler/big/<%#Eval("img_path") %>" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>

</asp:Content>
