﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="talepformu.aspx.cs" Inherits="mavi_yapi.talepformu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="icsayfa-serit">
        <p>KONUT  ÖN TALEP FORMU</p>
    </div>
    <div class="ic-govde mt20">
        <div class="span6 offset2">
            <asp:Label Text="" runat="server" ID="lblUyari"/>
            <table class="table table-kariyer">
                <tr class="bg-co-tr1">
                    <td style="width: 150px">Adı Soyadı</td>
                    <td style="width: 50px">:</td>
                    <td><asp:TextBox runat="server" ID="txtAdSoyad" placeholder="Ad Soyad Giriniz" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Tel</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTel" placeholder="Telefon numaranız" />
                    </td>
                </tr>
               
                <tr class="bg-co-tr1">
                    <td>E-Mail</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEPosta" placeholder="E-mail giriniz" />
                    </td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Proje Adı</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtProjeAdi" placeholder="Proje Adını giriniz." />
                    </td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>Daire Tipi</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtDaireTipi" placeholder="Daire Tipini giriniz." /></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                <asp:Button ID="btnGonder" CssClass="btn btn-private ml150" Text="Gönder" runat="server" OnClick="btnGonder_Click" Style="margin-bottom: 12px;" /></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
