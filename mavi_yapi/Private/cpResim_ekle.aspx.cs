﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpResim_ekle : cBase
    {
        int galeriID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["galeriID"] != null)
                galeriID = Convert.ToInt16(Request.QueryString["galeriID"]);

        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            maviyapi_resim yeniRef = new maviyapi_resim();
            string randomname = CreateRandomValue(10, true, true, true, false);
            //int lastSira = Convert.ToInt32(db.aktema_banner.LastOrDefault().sira);
            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/projeler/icerik/" + randomname + ".jpg"));

                    if (txtBaslik.Text != "")
                        yeniRef.baslik = txtBaslik.Text;

                    yeniRef.galeri_id = galeriID;


                    yeniRef.adi = randomname + ".jpg";
                    yeniRef.sira = 1;
                    db.maviyapi_resim.Add(yeniRef);
                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("cpResimler.aspx?galeriID="+galeriID);
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
}