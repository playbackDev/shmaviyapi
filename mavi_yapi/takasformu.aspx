﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="takasformu.aspx.cs" Inherits="mavi_yapi.takasformu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="icsayfa-serit">
        <p>ARSA KARŞILIĞI KONUT TALEP FORMU</p>
    </div>
    <div class="ic-govde mt20">
        <div class="span6 offset2">
            <asp:Label Text="" runat="server" ID="lblUyari"/>
            <table class="table table-kariyer">
                <tr class="bg-co-tr1">
                    <td style="width: 150px">Arsa Sahibi</td>
                    <td style="width: 50px">:</td>
                    <td><asp:TextBox runat="server" ID="txtAdSoyad" placeholder="Ad Soyad Giriniz" /></td>
                </tr>
                 <tr class="bg-co-tr2">
                    <td>İl/ilçe</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtYer" placeholder="İl/İlçe Giriniz." />
                    </td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>Tel</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTel" placeholder="Telefon numaranız" />
                    </td>
                </tr>
               
                <tr class="bg-co-tr2">
                    <td>E-Mail</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEPosta" placeholder="E-mail giriniz" />
                    </td>
                </tr>
                
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                <asp:Button ID="btnGonder" CssClass="btn btn-private ml150" Text="Gönder" runat="server" OnClick="btnGonder_Click" Style="margin-bottom: 12px;" /></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
