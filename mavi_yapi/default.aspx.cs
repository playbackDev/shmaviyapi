﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class _default : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var haberler = db.maviyapi_duyurular.Select(k => new
            {
                k.id,
                k.baslik
            }).ToList();

            rptHaberNoScript.DataSource = haberler;
            rptHaberNoScript.DataBind();

            rptHaberScript.DataSource = haberler;
            rptHaberScript.DataBind();


            var banner = db.maviyapi_banner.OrderBy(s => s.sira).ToList();
            rptBanner.DataSource = banner;
            rptBanner.DataBind();

            var galeri = db.maviyapi_galeri.OrderBy(x => Guid.NewGuid()).Take(7).ToList();
            rptGaleri.DataSource = galeri;
            rptGaleri.DataBind();

        }
    }
}