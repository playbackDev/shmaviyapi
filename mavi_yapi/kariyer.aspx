﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="kariyer.aspx.cs" Inherits="mavi_yapi.kariyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script src="assets/js/jquery.maskedinput.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
      <script type="text/javascript">
          $(function () {
              $.mask.definitions['~'] = "[+-]";
              $("#ContentPlaceHolder1_txtTel").mask("(999) 999 9999", { placeholder: "_ " });
              $("#ContentPlaceHolder1_txtCep").mask("(999) 999 9999", { placeholder: "_ " });
              $("#ContentPlaceHolder1_txtDogumTarihi").mask("99-99-9999", { placeholder: "_ " });
              $("#ContentPlaceHolder1_txtIseBaslamaTarihi").mask("99-99-9999", { placeholder: "_ " });
              $("#ContentPlaceHolder1_txtIsBitisTarihi").mask("99-99-9999", { placeholder: "_ " });
          });

          $(document).ready(function () {
              $(':input').autotab_magic();
          });
    </script>
    <div class="icsayfa-serit">
        <p>KARİYER</p>
    </div>
    <div class="ic-govde mt20">
         <%--<div class="span12">--%>
            <asp:Literal Text="" ID="lblUyari" runat="server" />
            <table class="table table-kariyer">
                <tr class="bg-co-tr1">
                    <td style="width: 120px;">Adı</td>
                    <td>:</td>
                    <td style="width: 250px;">
                        <asp:TextBox runat="server" ID="txtAdi" /></td>
                    <td style="width: 120px;">Soyadı</td>
                    <td>:</td>
                    <td style="width: 250px;">
                       <asp:TextBox runat="server" ID="txtSoyadi" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Telefon</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtTel" /></td>
                    <td>Cep Tel</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtCep" /></td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>E-Posta</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtEposta" /></td>
                    <td>Başvuru Tipi</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtBasvuruTipi" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Doğumtarihi</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtDogumTarihi" /></td>
                    <td>Cinsiyeti</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtCinsiyet" /></td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>İkamet Ettiği il</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIkametIl" /></td>
                    <td>Mazun olunan<br />
                        Okul/Bölüm</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtOkul" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>İş Yeri</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIsyeri" /></td>
                    <td>Görev</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtGorev" /></td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>İşe Başlama T.</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIseBaslamaTarihi" /></td>
                    <td>İş bitiş T.</td>
                    <td>:</td>
                    <td><asp:TextBox runat="server" ID="txtIsBitisTarihi" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Mesajınız</td>
                    <td>:</td>
                    <td colspan="4"><asp:TextBox runat="server" ID="txtMesaj" TextMode="MultiLine" CssClass="mesaj" Rows="5" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <asp:Button Text="Gönder" CssClass="btn btn-private" ID="btnGonder" runat="server" OnClick="btnGonder_Click"  /></td>
                </tr>
            </table>
        </div>
    <%--</div>--%>
</asp:Content>
