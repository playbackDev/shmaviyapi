//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mavi_yapi
{
    using System;
    using System.Collections.Generic;
    
    public partial class maviyapi_genelozellik_proje
    {
        public int id { get; set; }
        public Nullable<int> genel_ozellik_id { get; set; }
        public string genel_ozellik_degeri { get; set; }
        public Nullable<int> proje_id { get; set; }
    
        public virtual maviyapi_genel_ozellikler maviyapi_genel_ozellikler { get; set; }
        public virtual maviyapi_proje maviyapi_proje { get; set; }
    }
}
