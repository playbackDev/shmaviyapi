﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class proje_genel_ozellikleri : cBase
    {
        int ID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"].isNumara())
            {
                ID = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    List<alt_ozellikler> ozellik_listesi = new List<alt_ozellikler>();
                    var get_ozellikler = db.maviyapi_genelozellik_proje.Where(i => i.proje_id == ID).ToList();
                    if (get_ozellikler != null && get_ozellikler.Count() > 0)
                    {
                        foreach (maviyapi_genelozellik_proje item in get_ozellikler)
                        {
                            int id = Convert.ToInt32(item.id);
                            string prop = item.maviyapi_genel_ozellikler.ozellik.ToString();
                            ozellik_listesi.Add(new alt_ozellikler { ID=id, ozellik = prop, degeri = item.genel_ozellik_degeri });

                        }
                    }

                    rptOzellikler.BindVeri(ozellik_listesi.ToList());

                    List<olmayan_ozellikler> olmayanlar = new List<olmayan_ozellikler>();
                    var genel_ozelliklerr = db.maviyapi_genel_ozellikler.Where(i=>i.is_active==true).ToList();

                    foreach (maviyapi_genel_ozellikler item in genel_ozelliklerr)
                    {
                        var ozellikControl = db.maviyapi_genelozellik_proje.Where(o=>o.proje_id==ID&&o.genel_ozellik_id==item.id).FirstOrDefault();
                        if (ozellikControl==null)
                        {
                            olmayanlar.Add(new olmayan_ozellikler { ID=item.id, ozellik=item.ozellik });
                        }

                    }
                    drpAllOzelliks.BindVeri(olmayanlar.ToList(),"ozellik","ID",0,"Seçim Yapın");


                }
            }
            else
            {
                Response.Redirect("default.aspx", false);
            }
        }

        public class olmayan_ozellikler {
            public int ID { get; set; }
            public string ozellik { get; set; }
        }
        


        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var genel_ozellik = db.maviyapi_genelozellik_proje.Where(o => o.id == id).FirstOrDefault();
            db.maviyapi_genelozellik_proje.Remove(genel_ozellik);
            if (db.SaveChanges()>0)
            {
                RefreshPage();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            maviyapi_genelozellik_proje yeniozellik = new maviyapi_genelozellik_proje();
            if (txtOzellik.Text!="")
            {
                pnlGeneralError.Visible = false;
                yeniozellik.genel_ozellik_degeri = txtOzellik.Text;
                if (drpAllOzelliks.SelectedValue.isNumara())
                {pnlGeneralError.Visible = false;
                    yeniozellik.genel_ozellik_id = Convert.ToInt32(drpAllOzelliks.SelectedValue);
                    yeniozellik.proje_id = ID;
                    db.maviyapi_genelozellik_proje.Add(yeniozellik);
                    if (db.SaveChanges()>0)
                    {
                        RefreshPage();
                    }

                }
                else
                {
                    ltrErrorText.Text = "Lütfen bir özellik seçin.";
                    pnlGeneralError.Visible = true;

                }
                
            }
            else
            {
                ltrErrorText.Text = "Lütfen özelliğe bir değer girin.";
                pnlGeneralError.Visible = true;
            }

            
        }

        protected void lnkKaydet_Command(object sender, CommandEventArgs e)
        {
            LinkButton senderr = (LinkButton)sender;
            TextBox txtDegeri = (TextBox)senderr.Parent.FindControl("txtDegeri");

            int id = Convert.ToInt32(e.CommandArgument);
            var genel_ozellik = db.maviyapi_genelozellik_proje.Where(o => o.id == id).FirstOrDefault();
            if (txtDegeri.Text!="")
            {
                genel_ozellik.genel_ozellik_degeri = txtDegeri.Text;
                if (db.SaveChanges()>0)
                {
                    RefreshPage();
                }

            }


        }
    }
}