﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpKatalog_duzenle :cBase
    {
        int katalogId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                katalogId = Convert.ToInt32(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var katalog = db.maviyapi_katalog.Where(i => i.id == katalogId).FirstOrDefault();
                    if (katalog != null)
                    {
                        txtBaslik.Text=katalog.baslik;
                    }
                }
            }
        }

        protected void btnAnaSlide_Click(object sender, EventArgs e)
        {
            var katalog = db.maviyapi_katalog.Where(i => i.id == katalogId).FirstOrDefault();
            string randomname = CreateRandomValue(10, true, true, true, false);
            if (txtBaslik.Text != "")
            {
                katalog.baslik = txtBaslik.Text;
            }

            if (flArsivSlide.HasFile)
            {
                try
                {
                    flArsivSlide.SaveAs(Server.MapPath("~/uploads/katalog/" + randomname + ".pdf"));
                    katalog.adi = randomname + ".pdf";
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }

            if (db.SaveChanges() > 0)
            {
                Response.Redirect("cpKataloglar.aspx");
            }
        }
    }
}