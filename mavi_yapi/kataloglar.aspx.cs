﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class kataloglar : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var kataloglar = db.maviyapi_katalog.Where(s => s.is_active == true).OrderBy(s=>s.sira).ToList();
            rptKataloglar.DataSource = kataloglar;
            rptKataloglar.DataBind();


        }
    }
}