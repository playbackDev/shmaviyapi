﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpResimler.aspx.cs" Inherits="mavi_yapi.Private.cpResimler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .islemm {
            position: absolute;
            bottom: 3px;
            left: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="span12 basin" style="padding-bottom:10px;">
         <h5>Proje Resimleri</h5>
        <div class="row" style="margin-bottom:20px;">
            <asp:Repeater ID="rptAnaslider" runat="server" OnItemDataBound="rptAnaslider_ItemDataBound">
                <ItemTemplate>
                    <div class="span3" style="background-color:rgba(202, 196, 196, 0.35)">
                        <asp:LinkButton ID="lnkSil" CssClass="silme" runat="server" CommandName="sil" CommandArgument='<%#Eval("id") %>' Style="text-decoration: none; bottom: 7px; color: white;" OnCommand="lnkSil_Command" ToolTip="Sil">  <i class="icon-remove icon-large"></i> Sil &nbsp;</asp:LinkButton>
                        <div class="islemm-ref">
                            <asp:Literal ID="ltrInput" Text="" runat="server" />
                            <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                            <a href="cpResim_duzenle.aspx?id=<%#Eval("id") %>" class="btn btn-success">Düzenle</a>
                        </div>
                        <img class="silik-ref" src="/uploads/projeler/icerik/<%#Eval("adi") %>" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
         <asp:HyperLink ID="hypEkle" CssClass="btn btn-success" Text="Resim Ekle"  runat="server" />
    </div>
</asp:Content>
