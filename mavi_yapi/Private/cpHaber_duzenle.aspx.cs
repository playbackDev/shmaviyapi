﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpHaber_duzenle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                int ID = Convert.ToInt16(Request.QueryString["id"]);
                if (!Page.IsPostBack)
                {
                    var haber = db.maviyapi_duyurular.Where(s => s.id == ID).FirstOrDefault();
                    txtbaslik.Text = haber.baslik;
                    txtIcerik.Text = haber.icerik;
                    txtSira.Text = haber.sira.ToString();
                    if (haber.link!=null)
                    {
                        txtLink.Text = haber.link;
                    }

                }
            }

        }

        protected void btnekle_Click(object sender, EventArgs e)
        {
            int ID = Convert.ToInt16(Request.QueryString["id"]);
            var haber = db.maviyapi_duyurular.Where(s => s.id == ID).FirstOrDefault();
            haber.baslik = txtbaslik.Text;
            haber.icerik = txtIcerik.Text;
            haber.sira =Convert.ToInt16(txtSira.Text);
            if (txtLink.Text != "")
            {
                haber.link = txtLink.Text;
            }
            if (db.SaveChanges() > 0)
            {
                ltrUyari.Text = "<div class='alert alert-success'>" + "İşlem başarıyla gerçekleşti." + "</div>";

            }
        }
    }
}