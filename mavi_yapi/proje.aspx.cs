﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class proje : cBase
    {
        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.QueryString["id"].IsTextBoxEmptyOrNull())
            {
                id = Convert.ToInt16(Request.QueryString["id"]);
                var proje = db.maviyapi_proje.Where(s => s.id == id).FirstOrDefault();
                ltrBaslik.Text = proje.baslik;
                ltrAciklama.Text = proje.icerik;
                ltrAdres.Text = proje.adres;
                List<ozellikler> ozellik_listesi = new List<ozellikler>();
                int pid = proje.id;
                var get_ozellikler = db.maviyapi_genelozellik_proje.Where(i => i.proje_id == id && i.maviyapi_genel_ozellikler.is_active == true).ToList();
                if (get_ozellikler != null && get_ozellikler.Count() > 0)
                {
                    foreach (maviyapi_genelozellik_proje item in get_ozellikler)
                    {
                        string prop = item.maviyapi_genel_ozellikler.ozellik.ToString();
                        ozellik_listesi.Add(new ozellikler { ozellik = prop, degeri = item.genel_ozellik_degeri });

                    }
                }


                rptKategoriler.BindVeri(db.maviyapi_kategori.ToList());
                rptGenelOzellikler.BindVeri(ozellik_listesi.ToList());
                //rptBanner.BindVeri(db.maviyapi_proje_resimleri.Where(i => i.proje_id == id).OrderBy(o => o.sira).ToList());
              // rptGaleri.BindVeri(db.maviyapi_proje_resimleri.Where(i => i.proje_id == id).OrderBy(o => o.sira).ToList());

            }
        }

        protected void rptKategoriler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptAltOzellikler = (Repeater)e.Item.FindControl("rptAltOzellikler");
            int kategori_id = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem,"id"));
            var alt_ozellikler = db.maviyapi_kategori_ozellik.Where(i=>i.kategori_id==kategori_id).ToList();
            rptAltOzellikler.BindVeri(alt_ozellikler);

        }

        protected void rptAltOzellikler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrValue = (Literal)e.Item.FindControl("ltrValue");
            int ozellikID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem,"id"));
            string ozellik_name = DataBinder.Eval(e.Item.DataItem,"ozellik").ToString();
            var ozellikproje = db.maviyapi_kategori_ozellik_proje.Where(i=>i.proje_id==id&&i.ozellik_id==ozellikID).FirstOrDefault();
            if (ozellikproje!=null)
            {
                ltrValue.Text = "<img src='/assets/img/check.png'/>&nbsp;" + ozellik_name;
                
            }
            else
            {
                ltrValue.Text = "<img src='/assets/img/uncheck.png'/>&nbsp;" + ozellik_name;
            }

        }

        protected void lnkGonder_Click(object sender, EventArgs e)
        {
            string telefon = "Telefon Girilmedi";
           
            if (txtAd.Text.OnlyLetters(50))
            {
                ltruyarii.Text = "";
                if (txtMail.Text.IsEmail()&&txtMail.Text.Length<30)
                {
                    ltruyarii.Text = "";
                    if (!String.IsNullOrEmpty(txtTel.Text)&&txtTel.Text.Length<20)
                    {
                        telefon = txtTel.Text;
                    }

                    if (txtKonu.Text.OnlyLetters(100))
                    {
                        ltruyarii.Text = "";
                        if (!String.IsNullOrEmpty(txtMesaj.Text))
                        {
                            ltruyarii.Text = "";

                            string mail_msg = string.Empty;
                            mail_msg += "<table style='border:solid 1px #808080; color:#000000; background-color:#e2e2e2;'>";
                            mail_msg += "<tr><td colspan='2'><h3>Mail Bilgileri</h3></td></tr>";

                            mail_msg += "<tr><td>" + "Ad Soyad: " + "</td><td>" + txtAd.Text + "</td></tr>";
                            mail_msg += "<tr><td>" + "Konu: " + "</td><td>" + txtKonu.Text + "</td></tr>";
                            mail_msg += "<tr><td>" + "E-Mail: " + "</td><td>" + txtMail.Text + "</td></tr>";
                            mail_msg += "<tr><td>" + "IP: " + "</td><td>" + Request.UserHostAddress + "</td></tr>";
                            mail_msg += "<tr><td>" + "Tarih: " + "</td><td>" + DateTime.Now + "</td></tr>";

                            mail_msg += "<tr><td>" + "Mesaj : " + "</td><td>" + txtMesaj.Text + "</td></tr>";
                            mail_msg += "</table>";

                            if (mailGonder(txtMail.Text, mail_msg, txtKonu.Text)) {

                                pnlIlet.InputTemizle();
                                ltruyarii.Text = "<script>alert('Mesajınız İletilmiştir.');</script>";
                            }

                            
                        }
                        else
                        {
                            ltruyarii.Text = "<script>alert('Lütfen mesajınızı girin.');</script>";
                        }

                    }
                    else
                    {
                        ltruyarii.Text = "<script>alert('Sadece harflerden oluşmalıdır ve \n 100 karakteri geçmemelidir.');</script>";
                    }

                }
                else
                {
                    ltruyarii.Text = "<script>alert('Hatalı e-mail adresi.');</script>";
                }

            }
            else
            {
                ltruyarii.Text = "<script>alert('Lütfen adınızı girin.');</script>";
            }


        }

        public bool mailGonder(string email,string mesaj,string konu) {

            try
            {
                MailMessage mailMsg = new MailMessage();

                MailAddress mailAddress = new MailAddress("bilgi@shmaviyapi.com");
                MailAddress fromMail = new MailAddress(email);
                mailMsg.From = fromMail;
                mailMsg.To.Add(mailAddress);
                mailMsg.Subject = konu;
                mailMsg.Body = mesaj;
                mailMsg.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient("mail.shmaviyapi.com", 587);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("bilgi@shmaviyapi.com", "00545761");
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
                return true;
            }
            catch
            {
                return false;

            }
        
        }


    }
}