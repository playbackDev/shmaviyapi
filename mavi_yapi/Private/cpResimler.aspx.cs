﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpResimler : cBase
    {
        int galeriID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["galeriID"]!=null)
            {
                galeriID =Convert.ToInt16(Request.QueryString["galeriID"]);
                var resimler = db.maviyapi_resim.Where(s=>s.galeri_id==galeriID).OrderBy(p => p.sira).ToList();
                if (!Page.IsPostBack)
                {
                    if (resimler != null)
                    {
                        rptAnaslider.DataSource = resimler;
                        rptAnaslider.DataBind();

                        hypEkle.NavigateUrl = "cpResim_ekle.aspx?galeriID=" + galeriID;
                    }
                }
            }

        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var referansResimSil = db.maviyapi_resim.Where(o => o.id == id && o.galeri_id==galeriID ).FirstOrDefault();
            db.maviyapi_resim.Remove(referansResimSil);
            if (db.SaveChanges() > 0)
            {
                try
                {
                    File.Delete(Server.MapPath("~/uploads/projeler/icerik/" + referansResimSil.adi));
                }
                catch
                {
                }
                finally
                {
                    RefreshPage();
                }
            }
        }

        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.maviyapi_resim.Where(p => p.id == id && p.galeri_id==galeriID).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();
            }
            catch { }
            finally
            {
                cBase.RefreshPage();
            }
        }

        protected void rptAnaslider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";



        }
    }
}