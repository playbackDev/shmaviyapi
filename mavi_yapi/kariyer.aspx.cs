﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi
{
    public partial class kariyer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            string adi = txtAdi.Text;
            string soyadi = txtSoyadi.Text;
            string tel = txtTel.Text;
            string cep = txtCep.Text;
            string eposta = txtEposta.Text;
            string basvurutipi = txtBasvuruTipi.Text;
            string dogumtarihi = txtDogumTarihi.Text;
            string cinsiyet = txtCinsiyet.Text;
            string ikametili = txtIkametIl.Text;
            string okul = txtOkul.Text;
            string isyeri = txtIsyeri.Text;
            string gorev = txtGorev.Text;
            string isebaslamatarihi = txtIseBaslamaTarihi.Text;
            string isbitis = txtIsBitisTarihi.Text;
            string mesaj = txtMesaj.Text;

            string konu = "Kariyer";
            bool basarili = false;
            string uyariMetni = "";


            string mail_msg = string.Empty;
            mail_msg += "<table style='border:solid 1px #808080; color:#000000; background-color:#e2e2e2;'>";
            mail_msg += "<tr><td colspan='2'><h3>Mail Bilgileri</h3></td></tr>";
            mail_msg += "<tr><td>" + "Konu: " + "</td><td>" + konu + "</td></tr>";
            mail_msg += "<tr><td>" + "E-Mail: " + "</td><td>" + eposta + "</td></tr>";
            mail_msg += "<tr><td>" + "IP: " + "</td><td>" + Request.UserHostAddress + "</td></tr>";
            mail_msg += "<tr><td>" + "Tarih: " + "</td><td>" + DateTime.Now + "</td></tr>";

            mail_msg += "<tr><td colspan='2'><h3>Başvuru Bilgileri</h3></td></tr>";

            mail_msg += "<tr><td>" + "Ad : " + "</td><td>" + adi + "</td></tr>";
            mail_msg += "<tr><td>" + "Soyadı : " + "</td><td>" + soyadi + "</td></tr>";
            mail_msg += "<tr><td>" + "Tel : " + "</td><td>" + tel + "</td></tr>";
            mail_msg += "<tr><td>" + "Cep : " + "</td><td>" + cep + "</td></tr>";
            mail_msg += "<tr><td>" + "E Posta : " + "</td><td>" + eposta + "</td></tr>";
            mail_msg += "<tr><td>" + "Başvuru : " + "</td><td>" + basvurutipi + "</td></tr>";
            mail_msg += "<tr><td>" + "Doğum Tarihi : " + "</td><td>" + dogumtarihi + "</td></tr>";
            mail_msg += "<tr><td>" + "Cinsiyet : " + "</td><td>" + cinsiyet + "</td></tr>";
            mail_msg += "<tr><td>" + "İkamet İl : " + "</td><td>" + ikametili + "</td></tr>";
            mail_msg += "<tr><td>" + "Okul: " + "</td><td>" + okul + "</td></tr>";
            mail_msg += "<tr><td>" + "İş yeri : " + "</td><td>" + isyeri + "</td></tr>";
            mail_msg += "<tr><td>" + "Görev : " + "</td><td>" + gorev + "</td></tr>";
            mail_msg += "<tr><td>" + "İşe Başlama Tarihi : " + "</td><td>" + isebaslamatarihi + "</td></tr>";
            mail_msg += "<tr><td>" + "İş Bitiş Tarihi : " + "</td><td>" + isbitis + "</td></tr>";
            mail_msg += "<tr><td>" + "Mesaj : " + "</td><td>" + mesaj + "</td></tr>";
            mail_msg += "</table>";


            try
            {
                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add("bilgi@shmaviyapi.com");
                eposta = eposta.IsEmail() ? eposta : "bilgi@shmaviyapi.com";
                MailAddress mailAddress = new MailAddress(eposta);
                mailMsg.From = mailAddress;
                mailMsg.Subject = konu;
                mailMsg.Body = mail_msg;
                mailMsg.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient("mail.playback.com.tr", 587);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("mesaj@playback.com.tr", "msg123");
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
                uyariMetni = "Mesajınız Gönderilmiştir.";
                basarili = true;
            }
            catch
            {
                uyariMetni = "Mail Gönderilirken hata oluştu.";

            }


            if (basarili)

                lblUyari.Text = "<div class='alert alert-success'>" + uyariMetni + "</div>";
            else
                lblUyari.Text = "<div class='alert alert-error'>" + uyariMetni + "</div>";


        }
    }
}