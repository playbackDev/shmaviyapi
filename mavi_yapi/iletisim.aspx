﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="iletisim.aspx.cs" Inherits="mavi_yapi.iletisim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="assets/js/jquery.maskedinput.js"></script>
    <script src="assets/js/jquery.validate.js"></script>
      <script type="text/javascript">
          $(function () {
              $.mask.definitions['~'] = "[+-]";
              $("#ContentPlaceHolder1_txtTel").mask("(999) 999 9999", { placeholder: "_ " });
          });

          $(document).ready(function () {
              $(':input').autotab_magic();
          });
    </script>
    <div class="icsayfa-serit">
        <p>İLETİŞİM</p>
    </div>
    <div class="ic-govde mt20">
        <div class="span5">
            <asp:Label Text="" runat="server" ID="lblUyari"/>
            <table class="table table-kariyer">
                <tr class="bg-co-tr1">
                    <td style="width: 150px">Ad Soyad</td>
                    <td style="width: 50px">:</td>
                    <td><asp:TextBox runat="server" ID="txtAdSoyad" placeholder="Ad Soyad Giriniz" /></td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>E-posta</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEPosta" placeholder="E-mail giriniz" />
                    </td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>Telefon</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTel" placeholder="Telefon numaranız" />
                    </td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Konu</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtKonu" placeholder="ileti konusu" />
                    </td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>Mesaj</td>
                    <td>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMesaj" TextMode="MultiLine" placeholder="mesajınız." Rows="7" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                <asp:Button ID="btnGonder" CssClass="btn btn-private ml150" Text="Gönder" runat="server" OnClick="btnGonder_Click" Style="margin-bottom: 12px;" /></td>
                </tr>
            </table>
        </div>

        <div class="span5 ml40">
            <table class="table table-kariyer">
                <tr class="bg-co-tr1">
                    <td>İstanbul Ofis: </td>
                    <td>:</td>
                    <td>Cevizlik Mah. Hatboyu Cad. No:50/A<br />
                        Bakırköy /İstanbul
                        <br />
                        0(212) 466 51 19</td>
                </tr>
                <tr class="bg-co-tr2">
                    <td>Merkez Ofis: </td>
                    <td>:</td>
                    <td>Muhsin Yazıcıoğlu Bulvarı No: 30 Mavi Yapı İş Merkezi Kat:2
                        <br />
                        Serdivan /SAKARYA
                        <br />
                        0 (264) 277 78 77 </td>
                </tr>
                <tr class="bg-co-tr1">
                    <td>E-Posta</td>
                    <td>:</td>
                    <td>bilgi@shmaviyapi.com</td>
                </tr>
            </table>
        </div>
        <div class="span5 ml40">
            <iframe width="380" height="183" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?f=q&amp;source=s_q&amp;hl=tr&amp;geocode=&amp;q=muhsin,serdivan&amp;aq=&amp;sll=40.778787,30.399213&amp;sspn=0.020538,0.052314&amp;ie=UTF8&amp;hq=muhsin&amp;hnear=Sakarya,+T%C3%BCrkiye&amp;t=m&amp;ll=40.764486,30.369387&amp;spn=0.011896,0.03253&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://www.google.com/maps?f=q&amp;source=embed&amp;hl=tr&amp;geocode=&amp;q=muhsin,serdivan&amp;aq=&amp;sll=40.778787,30.399213&amp;sspn=0.020538,0.052314&amp;ie=UTF8&amp;hq=muhsin&amp;hnear=Sakarya,+T%C3%BCrkiye&amp;t=m&amp;ll=40.764486,30.369387&amp;spn=0.011896,0.03253&amp;z=14" style="color:#0000FF;text-align:left">Daha Büyük Görüntüle</a></small>

        </div>
    </div>
</asp:Content>
