﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="proje_ekle.aspx.cs" Inherits="mavi_yapi.Private.proje_ekle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        ul {
            list-style: none;
        }

            .lifloat {
                float: left;
                margin-right: 10px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5>Proje Ekle</h5>

    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
    <table class="table table-bordered ortala">
        <thead>

            <tr style="background-color: rgba(109, 143, 144, 0.62);">
                
                <th>Proje Başlık</th>
                <th>Proje Tipi</th>
                <th>Proje Kapak Resmi </th>
            </tr>
            <tr>
                <th>
                    <asp:TextBox ID="txtPrjBaslik" runat="server" /></th>
                <th>
                    <asp:DropDownList runat="server" ID="drpPrjTip">
                        <asp:ListItem Text="KONUT" Value="1" Selected="True" />
                        <asp:ListItem Text="TAAHHÜT" Value="2" />
                    </asp:DropDownList></th>

                <th>
                    <asp:FileUpload ID="fileAnaKategori" runat="server" />
                </th>
               
            </tr>
          <tr>
               
                <th colspan="3">Proje Adres
                </th>
            </tr>
            <tr>
               
                <th colspan="3">
                    <asp:TextBox ID="txtAdres" runat="server" TextMode="MultiLine" Height="200" />
                </th>
            </tr>

            <tr>
               
                <th colspan="3">Proje içerik
                </th>
            </tr>
            <tr>
               
                <th colspan="3">
                    <asp:TextBox ID="txtPrjIcerik" runat="server" TextMode="MultiLine" Height="200" />
                </th>
            </tr>
            <tr> <th>
                    <asp:Button Text="Ekle" ID="btnKategoriEkle" OnClick="btnKategoriEkle_Click" CssClass="btn btn-success" runat="server" /></th></tr>
        </thead>
    </table>

</asp:Content>
