﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mavi_yapi.panel
{
    public partial class users : cBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int ID = Convert.ToInt16(Session["userID"]);
                var user = db.maviyapi_admin.Where(d => d.ID == ID).FirstOrDefault();

                if (user != null)
                {
                    txtusername.Text = user.kullaniciAdi;
                    txtpass.Text = user.sifre.ToString();
                }
            }
        }

        protected void guncelle_Click(object sender, EventArgs e)
        {
            int ID = Convert.ToInt16(Session["userID"]);
            var neyiguncelliceksen = db.maviyapi_admin.Where(d => d.ID == ID).FirstOrDefault();
            neyiguncelliceksen.kullaniciAdi = txtusername.Text;
            neyiguncelliceksen.sifre = txtpass.Text;

            if (db.SaveChanges() > 0)
            {
                ltrsuccess.Text = "İşlem Başarısız oldu.";
                RefreshPage();
            
            }
            else
            {
                ltrsuccess.Text = "İşlem Başarısız oldu.";
            }

        }
    }
}