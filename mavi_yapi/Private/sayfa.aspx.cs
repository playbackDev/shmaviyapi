﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class sayfa : cBase
    {
        int pageID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"]!=null)
            {
                pageID = Convert.ToInt32(Request.QueryString["id"]);
                var sayda = db.maviyapi_sayfalar.Where(o=>o.id==pageID).FirstOrDefault();

                if (!Page.IsPostBack)
                {
                    if (sayda!=null)
                    {
                        txtICerik.Text = sayda.icerik;
                        ltrSayfaadi.Text = sayda.adi;
                    }
                }
                    
            }

        }

        protected void btnSayfaDuzenle_Click(object sender, EventArgs e)
        {
            var sayda = db.maviyapi_sayfalar.Where(o => o.id == pageID).FirstOrDefault();
            if (txtICerik.Text!="")
            {
                sayda.icerik = txtICerik.Text;
            }
            if (db.SaveChanges() > 0) {
                Response.Redirect("default.aspx");
           
            }
        }
    }
}