﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="kataloglar.aspx.cs" Inherits="mavi_yapi.kataloglar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="icsayfa-serit">
        <p>KATALOGLAR</p>
    </div>
    <div class="ic-govde mt20">
       
            <table class="table table-kariyer">
                
                    <asp:Repeater runat="server" ID="rptKataloglar">
                        <ItemTemplate>
                            <tr class="bg-co-tr1">
                                <td><a href="/Katalog/<%#Eval("id")%>"><%#Eval("baslik")%></a> </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                
            </table>
      
    </div>
</asp:Content>
