﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class proje_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                //rptGenelOzellikler.BindVeri(db.maviyapi_genel_ozellikler.ToList());
               // rptKategoriler.BindVeri(db.maviyapi_kategori.ToList());

            }


        }

        protected void btnKategoriEkle_Click(object sender, EventArgs e)
        {
            maviyapi_proje yeniGaleri = new maviyapi_proje();
            yeniGaleri.durum = false;

            if (txtPrjBaslik.Text != "")
            {
                yeniGaleri.baslik = txtPrjBaslik.Text;

                if (txtPrjIcerik.Text != "")
                    yeniGaleri.icerik = txtPrjIcerik.Text;
                yeniGaleri.tip = Convert.ToInt16(drpPrjTip.SelectedValue);

                pnlGeneralError.Visible = false;
                if (fileAnaKategori.HasFile)
                {
                    if (txtAdres.Text!="")
                    {
                        yeniGaleri.adres = txtAdres.Text;
                    }


                    try
                    {
                        fileAnaKategori.SaveAs(Server.MapPath("~/uploads/projeler/kapak/" + fileAnaKategori.FileName));
                        ResizeImage("~/uploads/projeler/kapak/" + fileAnaKategori.FileName, 200, 150);
                        yeniGaleri.resim = fileAnaKategori.FileName;
                        yeniGaleri.sira = 1;

                        db.maviyapi_proje.Add(yeniGaleri);
                        if (db.SaveChanges() > 0)
                        {
                            RefreshPage();
                        }

                    }
                    catch (Exception ex)
                    {
                        ltrErrorText.Text = "Hata: " + ex.Message;
                        pnlGeneralError.Visible = true;

                    }

                }
                else
                {
                    ltrErrorText.Text = "Lütfen Galeri resmi seçin";
                    pnlGeneralError.Visible = true;
                }
            }
            else
            {
                ltrErrorText.Text = "Lütfen Galeri adın girin.";
                pnlGeneralError.Visible = true;
            }
        }

        protected void rptKategoriler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int kategoriID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem,"id"));
            var ozellikler = db.maviyapi_kategori_ozellik.Where(i=>i.kategori_id==kategoriID).ToList();
            Repeater rptOzellikler = (Repeater)e.Item.FindControl("rptAltOZellikler");
            rptOzellikler.BindVeri(ozellikler);

        }
    }
}