﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpProje.aspx.cs" ValidateRequest="false" Inherits="mavi_yapi.Private.cpProje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .ortala td, th {
            text-align: center !important;
            vertical-align: middle !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h5>Projeler</h5>
    <asp:Panel runat="server" CssClass="alert alert-error" Visible="false" ID="pnlGeneralError">
        <asp:Literal ID="ltrErrorText" runat="server" />
    </asp:Panel>
   <%-- <table class="table table-bordered ortala">
        <thead>
            <tr style="background-color: rgba(109, 143, 144, 0.62);">
                <th>Tip Seçiniz</th>
                <th>Yönetim</th>
                <th>Durum Seçiniz</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>--%>
    <table class="table table-bordered ortala">
        <thead>
            <tr style="background-color: rgba(109, 143, 144, 0.62);">
                <th>ID</th>
                <th style="width: 200px;">Başlık</th>
                <th>Yönetim</th>
                <th>Proje Kapak Resmi</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptKategoriler" runat="server" OnItemDataBound="rptKategoriler_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("id") %></td>
                        <td><%#Eval("baslik") %></td>
                        <td>
                            <div class="btn-group">
                                <asp:LinkButton ID="lnkAktif" Text="" runat="server" CommandArgument='<%#Eval("id") %>' OnCommand="lnkAktif_Command" />
                                <asp:LinkButton ID="lnkDurum" Text="" runat="server" CommandArgument='<%#Eval("id") %>' OnCommand="lnkDurum_Command" />
                                <a href="cpResimler.aspx?galeriID=<%#Eval("id") %>" class="btn btn-success">Proje Resimleri</a> <a href="cpProje_duzenle.aspx?id=<%#Eval("id") %>" class="btn btn-inverse">Düzenle</a>
                            </div> <br />
                            <div class="btn-group">
                                <a href="proje_genel_ozellikleri.aspx?id=<%#Eval("id") %>" class="btn btn-primary">Genel Özellikler</a>
                                <a href="proje_ozellikleri.aspx?id=<%#Eval("id") %>" class="btn btn-danger">Proje Kategorileri</a>
                                <a href="proje_resimleri.aspx?id=<%#Eval("id") %>" class="btn">Proje Resimleri</a>

                                </div>
                            <br />
                            <br />
                            <div>

                                <asp:Literal ID="ltrInput" Text="" runat="server" />
                                <asp:LinkButton ID="lnkSirala" runat="server" OnCommand="lnkSirala_Command" CssClass="btn btn-info" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id") %>'>Sırala</asp:LinkButton>
                            </div>


                        </td>
                        <td>
                            <img src='/uploads/projeler/kapak/<%#Eval("resim") %>' width="220" height="150" />
                        </td>
                        </div>
                        

                    </tr>
                </ItemTemplate>
            </asp:Repeater>

        </tbody>
    </table>


</asp:Content>
