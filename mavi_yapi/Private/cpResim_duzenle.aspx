﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Panel.Master" AutoEventWireup="true" CodeBehind="cpResim_duzenle.aspx.cs" Inherits="mavi_yapi.Private.cpResim_duzenle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h5>Resim Düzenle</h5>

<div class="well">
        <button type="button" class="close" data-dismiss="alert" style="font-size:16px;">&times;Kapat</button>
       <strong>Dikkat!</strong> Yüklediğiniz resimlerin ham formatta olmamasına dikkat edin. Ham formattaki resimlerin boyutları çok büyük olacağından parformans açısından uygun değillerdir. <br />
    </div>
      <table class="table table-bordered table-striped">
            <thead> 
                <tr>                 
                    <th style="text-align: left; vertical-align: middle; width: 10%;">Resim Başlığı <asp:TextBox ID="txtBaslik" runat="server" placeholder="Resim Başlığı" /> <span style="color:red;">Maximum 250 Karakter girin!</span>   </th>
                </tr>
                <tr>
                   <th style="text-align: left; vertical-align: middle; width: 10%;"> <asp:FileUpload ID="flArsivSlide" runat="server" /> <asp:Button ID="btnAnaSlide" Text="Yükle" runat="server" CssClass="btn btn-success" OnClick="btnAnaSlide_Click" /> </th>
                </tr>
            </thead>
         </table>
</asp:Content>
