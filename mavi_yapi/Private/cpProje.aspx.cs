﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mavi_yapi.Private
{
    public partial class cpProje : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                var projeler = db.maviyapi_proje.OrderBy(p=>p.sira).ToList();
                if (projeler != null)
                {
                    rptKategoriler.DataSource = projeler;
                    rptKategoriler.DataBind();
                } 
            }
        }

        protected void rptKategoriler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkAktif");
            LinkButton lnkDurum = (LinkButton)e.Item.FindControl("lnkDurum");

            bool aktif = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));
            bool durum = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "durum"));
            if (aktif)
            {
                lnk.CssClass = "btn btn-info";
                lnk.Text = "Aktif";
            }
            else
            {
                lnk.CssClass = "btn btn-danger";
                lnk.Text = "Pasif";
            }

            if (durum)
            {
                lnkDurum.CssClass = "btn btn-info";
                lnkDurum.Text = "Tamamlanan";
            }
            else
            {
                lnkDurum.CssClass = "btn btn-danger";
                lnkDurum.Text = "Devam Eden";
            }
           
            Literal input = (Literal)e.Item.FindControl("ltrInput");
            input.Text = "<input type=\"text\" name=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" id=\"txtSira" + DataBinder.Eval(e.Item.DataItem, "id") + "\" class=\"span1\" style='margin:0px;' value=\"" + DataBinder.Eval(e.Item.DataItem, "sira") + "\" />";


        }

       

        protected void lnkAktif_Command(object sender, CommandEventArgs e)
        {
            int katID = Convert.ToInt32(e.CommandArgument);
            var galeri = db.maviyapi_proje.Where(o => o.id == katID).FirstOrDefault();
            if (Convert.ToBoolean(galeri.is_active) == true)
            {
                galeri.is_active = false;
            }
            else
            {
                galeri.is_active = true;
            }
            if (db.SaveChanges() > 0)
            {
                RefreshPage();
            }

        }

        protected void lnkDurum_Command(object sender, CommandEventArgs e)
        {
            int katID = Convert.ToInt32(e.CommandArgument);
            var galeri = db.maviyapi_proje.Where(o => o.id == katID).FirstOrDefault();
            if (Convert.ToBoolean(galeri.durum) == true)
            {
                galeri.durum = false;
            }
            else
            {
                galeri.durum = true;
            }
            if (db.SaveChanges() > 0)
            {
                RefreshPage();
            }

        }
  

        protected void lnkSirala_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int sira = Convert.ToInt32(Request.Form["txtSira" + e.CommandArgument]);
                int id = Convert.ToInt32(e.CommandArgument);
                var sirala = db.maviyapi_proje.Where(p => p.id == id).FirstOrDefault();
                sirala.sira = sira;
                db.SaveChanges();
            }
            catch { }
            finally
            {
                cBase.RefreshPage();
            }
        }
    }
}